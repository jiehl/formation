# corrections
quelques idées, en tout cas.

on commence par les cas dégénérés que je n'ai pas montré pendant la formation : ou pourquoi s'embette-t-on à utiliser la même séquence par pixel, mais avec des permuations différentes ? alors qu'il y a des solutions plus directes...


## séquence Sobol' globale sur l'image
par exemple, on peut exploiter la propriété de stratification forte des 2 premières dimensions de Sobol' qui forment une (0, 2)-séquence, ce qui veut dire que pour chaque puissance de 2, la séquence visite chaque point une seule fois (rappel : t= 0 donc $2^t=1$ point dans chaque strate...)

comment profiter de cette propriété ? 
il faut reprendre la boucle de rendu de l'image, elle n'est pas compatible avec ce que l'on va faire :

```
version classique par pixel :
for(py < height)
for(px < width)
    Sampler rng( seed )
    for(i < samples)
        rng.index(i)
        // genere le ieme sample du pixel
        float x= rng.sample()
        float y= rng.sample()
        // genere le rayon pour le point (px+x, py+y) sur le plan image
```

pour la transformer en :
```
Sobol rng
for(i < samples*width*height)
    rng.index(i)
    float x= rng.sample()
    float y= rng.sample()
    // verifie que le ieme sample correspond à un pixel de l'image
    // genere le rayon pour le point (x * width, y * height) sur le plan image
```

si l'image est carrée et son coté est une puissance de 2, tous les points correspondent à un pixel. sinon, il faut trouver la puissance de 2 pour se retrouver dans le bon cas et éliminer tous les points qui ne correspondent pas à un pixel.

les détails pour les curieux sont dans [`projects/path_sobol.cpp`](projects/path_sobol.cpp)

mais voila le résultat en images :

1 point :\
![](figures/sobol-global-0001.png){ width=75% }

2 points :\
![](figures/sobol-global-0002.png){ width=75% }

4 points :\
![](figures/sobol-global-0004.png){ width=75% }

8 points :\
![](figures/sobol-global-0008.png){ width=75% }

16 points :\
![](figures/sobol-global-0016.png){ width=75% }

bien sur l'estimateur est le même que d'habitude, il convergera vers une image propre, mais les premiers points créent de grosses structures dans l'image qui seront difficiles à filtrer.


### même séquence par pixel 
une solution alternative, est d'utiliser une séquence par pixel. que se passe-t-il si on construit mal les permutations de la séquence, ou si on utilise la séquence directement ?

pour reproduire les exemples, il suffit d'utiliser le sampler Sobol et son contructeur par défaut. par exemple, dans ao.cpp, il suffit de remplacer :
```
Sampler rng( seed );
```
par
```
Sobol rng; // meme sequence par pixel
```

voila les résultats, sans trop de surprise, c'est moche !!

1 point :\
![](figures/sequence-0001.png){ width=75% }

2 points :\
![](figures/sequence-0002.png){ width=75% }

4 points :\
![](figures/sequence-0004.png){ width=75% }

8 points :\
![](figures/sequence-0008.png){ width=75% }

16 points :\
![](figures/sequence-0016.png){ width=75% }


### bilan
on comprend mieux pourquoi on s'embette avec les permutations d'Owen ou les permutations approchées (cf RDS)...


