
#include <cstdio>
#include <cstring>
#include <cfloat>
#include <algorithm>


#include "image.h"
#include "image_io.h"


struct cell
{
    Image image;
    
    const char *filename;
    const char *title;

    int x, y;
    int nspp;
    int time;
};


// 
int render_nspp( const char *filename )
{
    int n= -1;
    
    // suppose que le nom de fichier est de la forme 'prefix-nspp.[pfm|png]'
    const char *ext= strrchr(filename, '.');
    if(ext)
    {
        if(strcmp(ext, ".pfm") == 0 || strcmp(ext, ".png") == 0 )
        {
            const char *last= strrchr(filename, '-');
            if(sscanf(last, "-%d.", &n) == 1)
                return n;
        }
    }
    
    printf("\n[error] filename '%s'... doesn't match 'prefix-nspp.[pfm|png]'\n", filename);
    return 0;
}


unsigned render_time( const char *filename )
{
    unsigned d= 0;
    
    // suppose que le nom de fichier est de la forme 'prefix-time-nspp.[pfm|png]'
    const char *ext= strrchr(filename, '.');
    if(ext)
    {
        if(strcmp(ext, ".pfm") == 0 || strcmp(ext, ".png") == 0 )
        {
            char tmp[1024];
            strcpy(tmp, filename);
            char *last= strrchr(tmp, '-');
            if(last)
            {
                *last= 0;
                const char *prev= strrchr(tmp, '-');
                if(prev)
                {
                    if(sscanf(prev, "-%d", &d) == 1)
                        return d;
                }
            }
        }
    }
    
    //~ printf("\n[warning] filename '%s'... doesn't match 'prefix-time-nspp.[pfm|png]'\n", filename);
    return 0;
}


// mse d'une vignette 
std::pair<double, double> Mlocal( /* ref */ const Image &A, /* image */ const Image &B,
    int mX, int mY,
    int MX, int MY ,
    double &vmin, double &vmax)
{
    double val= 0.0;
    double var= 0.0;
    vmax= -1.0;
    vmin= 100000.0;
    
    int cpt= 0;
    for(int i=mX; i <= MX; ++i)
    for(int j=mY; j <= MY; ++j)
    {
        Color d= A(i, j) - B(i, j);
        double v= (std::abs(d.r) + std::abs(d.g) + std::abs(d.b)) / 3;
        
        vmax= std::max(vmax, v);
        // ne compte pas les pixels noirs
        Color pixel= A(i, j);
        if(pixel.r + pixel.g + pixel .b  > 0) 
            vmin= std::min(vmin, v); 
        
        val += v;
        var += v*v;
        cpt++;
    }
    
    val /= double(cpt);
    var /= double(cpt);
    var= (var - val*val);
    return std::make_pair(val, var);
}


Image read( const char *filename )
{
    if(pfm_image(filename))
        return read_image_pfm(filename);
        
    return read_image(filename);
}


/*! renvoie le chemin d'acces a un fichier. le chemin est toujours termine par /
    pathname("path/to/file") == "path/to/"
    pathname("file") == "./"
 */
std::string path_name( const std::string& filename )
{
    std::string path= filename;
#ifndef WIN32
    std::replace(path.begin(), path.end(), '\\', '/');   // linux, macos : remplace les \ par /.
    size_t slash = path.find_last_of( '/' );
    if(slash != std::string::npos)
        return path.substr(0, slash +1); // inclus le slash
    else
        return "./";
#else
    std::replace(path.begin(), path.end(), '/', '\\');   // windows : remplace les / par \.
    size_t slash = path.find_last_of( '\\' );
    if(slash != std::string::npos)
        return path.substr(0, slash +1); // inclus le slash
    else
        return ".\\";
#endif
}


std::string normalize_path( const std::string& file )
{
    std::string tmp= file;
    
#ifndef WIN32
    std::replace(tmp.begin(), tmp.end(), '\\', '/');   // linux, macos : remplace les \ par /.
#else
    std::replace(tmp.begin(), tmp.end(), '/', '\\');   // windows : remplace les / par \.
#endif
    return tmp;
}

std::string base_name( const std::string& filename )
{
    std::string nfilename= normalize_path(filename);
    size_t slash = nfilename.find_last_of( '/' );
    if(slash != std::string::npos)
        return nfilename.substr(slash +1);
    else
        return nfilename;
}

std::string change_extension( const std::string& filename, const std::string& ext )
{
    size_t pos= filename.rfind(".");
    
    return filename.substr(0, pos).append(ext);
}

std::string change_prefix( const std::string& prefix, const std::string& filename )
{
    std::string path= path_name(filename);
    std::string file= base_name(filename);
    
    path.append(prefix).append(file);
    return path;
}


int main( int argc, const char *argv[] )
{
    if(argc < 2)
    {
        printf("usage: %s reference.[pfm|png] --line 1 [--title \"...\"] image.[pfm|png] [--line 2 images --line 3 images] \n", argv[0]);
        return 0;
    }
    
    //
    Image reference;
    if(argc > 1)
        reference= read(argv[1]);
        
    if(reference.size() == 0)
    {
        printf("[error] loading reference image '%s'...\n", argv[1]);
        return 1;
    }
    
    // charge les images
    int rows= 0;
    int columns= 0;
    std::vector< std::vector<cell> > grid;
    {
        std::vector<cell> cells;
        
        int x= 0;
        int y= -1;
        int last_row= -1;
        const char *last_title= nullptr;
        
        for(int option= 2; option < argc; )
        {
            if(argv[option][0] == '-')
            {
                // nouvelle ligne
                if(strcmp(argv[option], "--line") == 0 && option +1 < argc)
                {
                    int row= 0;
                    if(sscanf(argv[option+1], "%d", &row) != 1)
                        break;
                    
                    x= 0;
                    //~ y++;
                    y= row -1;
                    option+= 2;
                    
                    printf("line %d\n", row);
                }
                else if(strcmp(argv[option], "--title") == 0 && option +1 < argc)
                {
                    last_title= argv[option+1];
                    option+= 2;
                }
                else
                {
                    printf("[error] parsing option '%s'...\n", argv[option]);
                    return 1;
                }
            }
            else
            {
                const char *filename= argv[option];
                int n= render_nspp(filename);
                int render= render_time(filename);
                cells.push_back( {Image(), filename, last_title, x, y, n, render} );
                
                printf("x %d y %d: '%s' %s, nspp %d, render %dms\n", x, y, last_title, filename, n, render);
                
                x++;
                option++;
            }
        }
        
        // chargement parallele des images
    #pragma omp parallel for
        for(int i= 0; i < int(cells.size()); i++)
            cells[i].image= read(cells[i].filename);
        
        // dimensions de la grille
        for(auto const & cell : cells)
        {
            rows= std::max(rows, cell.y +1);
            columns= std::max(columns, cell.x +1);
        }
        
        // cree la grille
        grid.resize(rows);
        for(int i= 0; i < rows; i++)
            grid[i].resize(columns);
        
        // copie les images a leur place
        for(auto const & cell : cells)
        {
            int x= cell.x;
            int y= cell.y;
            assert(x < columns);
            assert(y < rows);
            grid[y][x]= cell;   // move ?
        }
    }
    
    // mse global
#pragma omp parallel for schedule(dynamic, 1)
    for(int r= 0; r < rows; r++)
    {
        if(!grid[r][0].title)
            continue;
        
        printf("mse %s\n", grid[r][0].title);
        char filename[1024];
        sprintf(filename, "%smse_%s.txt", path_name(grid[r][0].filename).c_str(), grid[r][0].title);
        
        //~ printf("mse file '%s'\n", filename);
        FILE *out= fopen(filename, "wt");
        if(out)
        {
            printf("writing '%s'...\n", filename);
            
            //~ fprintf(out, "# %s\n", grid[r][0].title);
            fprintf(out, "#Nbpts    #Mean   #Min    #Max\n");
            for(int c= 0; c < columns; c++)
            {
                if(grid[r][c].image.size() == 0)
                    continue;
                    
                double vmin, vmax;
                std::pair<double, double> stat= Mlocal(reference, grid[r][c].image, 0, 0, grid[r][c].image.width()-1, grid[r][c].image.height()-1, vmin, vmax);
                
                double mse= stat.first;
                fprintf(out, "%d %.15f %.15f %.15f\n", grid[r][c].nspp, mse, vmin, vmax);
                //~ fprintf(out, "%d %.15f %.15f %.15f\n", grid[r][c].time, mse, vmin, vmax);
            }
            
            fclose(out);
        }
    }
    
    return  0;
}
