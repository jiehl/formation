
#ifndef _SAMPLER_SOBOL_H
#define _SAMPLER_SOBOL_H

#include <array>

#include "sampler.h"
#include "owen_fast.h"
#include "sobol_matrices.h"


struct Sobol
{
    Sobol( ) = default;
    
    Sobol( const unsigned seed ) : n(0), d(0), scrambles()
    {
        FCRNG rng(seed);
        for(unsigned d= 0; d < sobol_matrices_size; d++)
            scrambles[d]= rng.sample_int();
    }
    
    Sobol& index( const unsigned i ) 
    { 
        n= i; 
        d= 0; 
        return *this; 
    }
    
    float sample( ) 
    {
        float x= sobol_sample_float(d, n, scrambles[d]);
        d++; 
        return x; 
    }

    unsigned n;
    unsigned d;
    
protected:
    std::array<unsigned, sobol_matrices_size> scrambles;
};


struct SobolGray
{
    SobolGray( ) = default;
    
    SobolGray( const unsigned seed ) : n(0), d(0), scrambles(), points(), code(0), bit(0)
    {
        FCRNG rng(seed);
        for(unsigned d= 0; d < sobol_matrices_size; d++)
            scrambles[d]= rng.sample_int() ;
    }
    
    SobolGray& index( const unsigned i ) 
    {
        assert(i == 0 || i == n+1);
        
        n= i; 
        d= 0;
        
        // previous state
        if(i == 0) 
            return *this;
        
        unsigned ncode= graycode(i);
        bit= trailing_zeroes(code ^ ncode);
        code= ncode;
        return *this; 
    }
    
    float sample( ) 
    {
        if(n > 0) 
            points[d]= points[d] ^ sobol_matrices[d][bit];
        
        float x= uint_to_float(points[d] ^ scrambles[d]);
        d++; 
        return x; 
    }

    unsigned n;
    unsigned d;
    
protected:
    std::array<unsigned, sobol_matrices_size> scrambles;
    std::array<unsigned, sobol_matrices_size> points;
    unsigned code;
    unsigned bit;
};


struct Owen
{
    Owen( ) = default;
    
    Owen( const unsigned seed ) : n(0), d(0), scrambles()
    {
        FCRNG rng(seed);
        for(unsigned d= 0; d < sobol_matrices_size; d++)
            scrambles[d]= rng.sample_int();
    }
    
    Owen& index( const unsigned i ) 
    { 
        n= i; 
        d= 0; 
        return *this; 
    }
    
    float sample( ) 
    {
        float x= uint_to_float( owen_scramble( sobol_sample(d, n), scrambles[d]) );
        d++; 
        return x; 
    }

    unsigned n;
    unsigned d;

protected:
    std::array<unsigned, sobol_matrices_size> scrambles;
};


struct OwenFast
{
    OwenFast( ) = default;
    
    OwenFast( const unsigned seed ) : n(0), d(0), scrambles()
    {
        FCRNG rng(seed);
        for(unsigned d= 0; d < sobol_matrices_size; d++)
            scrambles[d]= rng.sample_int();
    }
    
    OwenFast& index( const unsigned i ) 
    { 
        n= i; 
        d= 0; 
        return *this; 
    }
    
    float sample( ) 
    {
        float x= uint_to_float( owen_scramble_fast4( sobol_sample(d, n), scrambles[d]) );
        d++; 
        return x; 
    }

    unsigned n;
    unsigned d;
    
protected:
    std::array<unsigned, sobol_matrices_size> scrambles;
};

#endif
