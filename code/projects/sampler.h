
#ifndef _SAMPLER_H
#define _SAMPLER_H


inline float uint_to_float( const unsigned x )
{
    return float(x >> 8) / float(1u << 24);
}

inline double uint_to_double( const unsigned x )
{
    return double(uint64_t(x) << 20) / double(uint64_t(1) << 52);
}


struct Squares
{
    uint64_t n;
    
    Squares( ) : n(0) {}
    Squares( const uint64_t first ) : n(first) {}
    
    Squares& index( const unsigned i ) { return *this; }
    float sample( ) { return uint_to_float( squares32(n++) ); }
    float sample( const uint64_t id ) { return uint_to_float( squares32(id) ); }
    
    inline unsigned squares32( const uint64_t n, const uint64_t key= 0xc8e4fd154ce32f6d )
    {
       uint64_t x, y, z;
       
       y = x = n * key; 
       z = y + key;
       x = x*x + y; x = (x>>32) | (x<<32);       /* round 1 */
       x = x*x + z; x = (x>>32) | (x<<32);       /* round 2 */
       x = x*x + y; x = (x>>32) | (x<<32);       /* round 3 */

       return (x*x + z) >> 32;                   /* round 4 */
    }
};


struct LCG
{
    std::default_random_engine rng;
    std::uniform_real_distribution<float> uniform;
    unsigned d;
    
    LCG( ) : rng(), uniform(), d(0) {}
    LCG( const unsigned seed ) : rng( hash(seed) ), uniform(), d(0) {}
    // seed est un compteur, pas du tout adapte comme init du lcg, mais hash(seed) est suffisament uniforme
    
    LCG& index( const unsigned i ) { d= 0; return *this; }
    float sample( ) { d++; return uniform(rng); }
    
    static unsigned hash( unsigned x )
    {
        x ^= x >> 16;
        x *= 0x21f0aaad;
        x ^= x >> 15;
        x *= 0xd35a2d97;
        x ^= x >> 15;
        return x;
    }    
};


// counter based rng, cf "Parallel Random Numbers: As Easy as 1, 2, 3"
// https://www.thesalmons.org/john/random123/
// le code est quand meme bien degueu et l'etat interne des generateurs est un peu trop gros.
// openrand a serieusement nettoye l'implem. cf https://github.com/msu-sparta/OpenRAND/blob/main/include/openrand/philox.h

// du coup, plus simple et de qualite raisonnable :
// "PRNS with 32 bits of state"
// https://marc-b-reynolds.github.io/shf/2017/09/27/LPRNS.html
// pas les memes constantes que dans le blog, mais small crush passe sans probleme, donc j'imagine que ca suffit.
// et c'est mieux que std::default_random_engine qui echoue a la moitiee des tests de small crush...
struct FCRNG
{
    unsigned n;
    unsigned key;
    
    FCRNG( ) : n(0), key() { seed(123451); }
    FCRNG( const unsigned s ) : n(), key() { seed(s); }
    
    void seed( const unsigned s ) { n= 0; key= (s << 1) | 1u; }
    
    FCRNG& index( const unsigned i ) { n= i; return *this;}
    
    unsigned sample_int( ) { return hash(++n * key); }
    float sample( ) { return uint_to_float( sample_int() ); }
    
    unsigned sample_range( const unsigned range )
    {
        // Efficiently Generating a Number in a Range
        // cf http://www.pcg-random.org/posts/bounded-rands.html
        unsigned divisor= (uint64_t(1) << 32) / range;
        if(divisor == 0) return 0;
        
        while(true)
        {
            unsigned x= sample_int() / divisor;
            if(x < range) return x;
        }
    }
    
    // c++ interface
    unsigned operator() ( ) { return sample_int(); }
    static constexpr unsigned min( ) { return 0; }
    static constexpr unsigned max( ) { return (uint64_t(1) << 32) -1 ;}
    typedef unsigned result_type;
    
    static unsigned hash( unsigned x )
    {
        x ^= x >> 16;
        x *= 0x21f0aaad;
        x ^= x >> 15;
        x *= 0xd35a2d97;
        x ^= x >> 15;
        return x;
    }
    // cf "hash prospector" https://github.com/skeeto/hash-prospector/blob/master/README.md
};


#endif
