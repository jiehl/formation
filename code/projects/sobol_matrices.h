
#pragma once

#include <cassert>
#include <cstdint>
#include <array>


unsigned sobol_sample( const std::array<unsigned, 32>& m, unsigned n )
{
    unsigned x= 0;
    for(unsigned i= 0; n; i++, n>>= 1)
    {
        if(n & 1)
            x ^= m[i];
    }
    
    return x;
}

unsigned graycode( const unsigned i )
{
    return i ^ (i >> 1);
}

#ifdef _MSC_VER
#include <intrin.h>

unsigned trailing_zeroes( const unsigned i )
{
    unsigned long b = 0;
    _BitScanForward(&b, i); // vs 2022
    return b;
}
#else

unsigned trailing_zeroes( const unsigned i )
{
    assert(i > 0);
    return __builtin_ctz(i); // gcc
}

// todo et les pour les mac ?
#endif

unsigned sobol_sample_graycode( const std::array<unsigned, 32>& m, unsigned n, const unsigned prev )
{
    assert(n > 0);
    unsigned g= trailing_zeroes(graycode(n-1) ^ graycode(n));
    return prev ^ m[g];
}


//~ constexpr unsigned sobol_matrices_size= 16;
constexpr unsigned sobol_matrices_size= 128;
extern std::array<unsigned, 32> sobol_matrices[sobol_matrices_size];

unsigned sobol_sample( const unsigned d, const unsigned index )
{
    assert(d < sobol_matrices_size);
    return sobol_sample(sobol_matrices[d], index);
}

float sobol_sample_float( const unsigned d, const unsigned index )
{
    assert(d < sobol_matrices_size);
    return uint_to_float( sobol_sample(d, index) );
}

double sobol_sample_double( const unsigned d, const unsigned index )
{
    assert(d < sobol_matrices_size);
    return uint_to_double( sobol_sample(d, index) );
}

unsigned sobol_sample( const unsigned d, const unsigned index, const unsigned scramble )
{
    assert(d < sobol_matrices_size);
    return sobol_sample(sobol_matrices[d], index) ^ scramble;
}

float sobol_sample_float( const unsigned d, const unsigned index, const unsigned scramble )
{
    assert(d < sobol_matrices_size);
    return uint_to_float( sobol_sample(d, index, scramble) );
}

double sobol_sample_double( const unsigned d, const unsigned index, const unsigned scramble )
{
    assert(d < sobol_matrices_size);
    return uint_to_double( sobol_sample(d, index, scramble) );
}

