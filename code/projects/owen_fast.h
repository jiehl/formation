
#ifndef OWEN_FAST_H
#define OWEN_FAST_H

#include <cstdint>


// renvoie 0 ou 1, valeur du ieme bit de x
inline unsigned owen_bit( const unsigned x, const unsigned position )
{
    return (x >> position) & 1u;
}


// permute les bits 0 .. 31 de a
// meme construction que OwenScrambler de pbrt 4, mais plus decompose, plus lisible et plus rapide... 
// cf https://github.com/mmp/pbrt-v4/blob/39e01e61f8de07b99859df04b271a02a53d9aeb2/src/pbrt/util/lowdiscrepancy.h#L240
unsigned owen_scramble( const unsigned a, const unsigned seed )
{
    FCRNG rng(seed);

    // 
    unsigned node_index= 0;
    unsigned flip= rng.index(node_index).sample_range(2);
    unsigned digit= owen_bit(a, 31);
    unsigned b= (digit ^ flip) << 31;
    
    for(unsigned i= 1; i < 32; i++)
    {
        node_index= 2*node_index + digit +1;
        // heap layout, root i= 0, left 2i+1, right 2i+2
        
        flip= rng.index(node_index).sample_range(2);
        digit= owen_bit(a, 31 - i);
        b= b | (digit ^ flip) << (31 - i);
    }
    
    return b;
}

// permutation inverse
unsigned owen_inverse_scramble( const unsigned b, const unsigned seed )
{
    FCRNG rng(seed);
    
    //
    unsigned node_index= 0;
    unsigned flip= rng.index(node_index).sample_range(2);
    unsigned digit= owen_bit(b, 31) ^ flip;
    unsigned a= digit << 31;
    
    for(unsigned i= 1; i < 32; i++)
    {
        node_index= 2*node_index + digit +1;
        // heap layout, root i= 0, left 2i+1, right 2i+2
        
        unsigned flip= rng.index(node_index).sample_range(2);
        digit= owen_bit(b, 31 - i) ^ flip;
        a= a | digit << (31 - i);
    }
    
    return a;
}


// permute les bits 0 .. nbits de a
unsigned owen_scramble( const unsigned a, const unsigned seed, const unsigned nbits )
{
    FCRNG rng(seed);
    
    // 
    unsigned node_index= 0;
    unsigned flip= rng.index(node_index).sample_range(2);
    unsigned digit= owen_bit(a, nbits);
    unsigned b= (digit ^ flip) << nbits;
    
    for(unsigned i= 1; i < nbits+1; i++)
    {
        node_index= 2*node_index + digit +1;
        // heap layout, root i= 0, left 2i+1, right 2i+2
        
        unsigned flip= rng.index(node_index).sample_range(2);
        digit= owen_bit(a, nbits - i);
        b= b | (digit ^ flip) << (nbits - i);
    }
    
    return b;
}


// cf FastOwenScrambler, pbrt 4
// https://github.com/mmp/pbrt-v4/blob/39e01e61f8de07b99859df04b271a02a53d9aeb2/src/pbrt/util/lowdiscrepancy.h#L221
inline unsigned reverse_bits( unsigned n ) 
{
    n = (n << 16) | (n >> 16);
    n = ((n & 0x00ff00ff) << 8) | ((n & 0xff00ff00) >> 8);
    n = ((n & 0x0f0f0f0f) << 4) | ((n & 0xf0f0f0f0) >> 4);
    n = ((n & 0x33333333) << 2) | ((n & 0xcccccccc) >> 2);
    n = ((n & 0x55555555) << 1) | ((n & 0xaaaaaaaa) >> 1);
    return n;
}

// permute tous les bits directement.
unsigned owen_scramble_fast4( const unsigned a, const unsigned seed )
{
    unsigned v= reverse_bits(a);
    v ^= v * 0x3d20adea;
    v += seed;
    v *= (seed >> 16) | 1;
    v ^= v * 0x05526c56;
    v ^= v * 0x53a22864;
    return reverse_bits(v);
}

#endif
