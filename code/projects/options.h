
#ifndef _OPTIONS_H
#define _OPTIONS_H

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>


struct Options
{
    const char *filename= nullptr;
    const char *orbiter_filename= nullptr;
    const char *prefix= "image";
    int samples= 32;
    int depth= 1;
    
    Options( const char **begin, const char **end ) : options(begin, end)
    {
        if(options.size() > 1)
        {
            filename= option(1, filename);
            filename= value("--scene", filename);
            orbiter_filename= value("--camera", orbiter_filename);
            
            prefix= value("--prefix", prefix);
            samples= value_int("-s", "32");
            depth= value_int("--length", "1");
        }
        
        if(options.size() == 1 || flag("--help") || flag("-h") || flag("--verbose") || flag("-v"))
        {
            printf("filename: '%s'\n", filename);
            printf("camera: '%s'\n", orbiter_filename);
            printf("output prefix: '%s'\n", prefix);
            printf("samples: %u\n", samples);
            printf("path length: %u\n", depth);
        }
    }
    
    
    Options() = default;
    Options( const Options& ) = delete;
    Options& operator= ( const Options& ) = default;
    
    // option placee
    const char *option( const unsigned position, const char *default_value )
    {
        if(position >= options.size())
            return default_value;
        if(options[position] == nullptr)
            return default_value;
        
        const char *value= options[position];
        if(value[0] == '-')
        {
            printf("invalid option: [%u] = '%s'\n", position, value);
            exit(1);
        }
        
        options[position]= nullptr;
        return value;
    }
    
    // recherche des options
    int find( const char *name )
    {
        for(int i= 0; i < int(options.size()); i++)
            if(options[i] && std::string(options[i]) == std::string(name))
                return i;
        return -1;
    }

    // cherche un flag, renvoie un bool
    bool flag( const char *name )
    {
        int index= find(name);
        if(index != -1)
            options[index]= nullptr;
        
        return (index != -1);
    }

    int find_value( const char *name, const int n= 1 )
    {
        int index= find(name);
        if(index < 0 || index + n >= int(options.size()))
            return -1;
        
        for(int i= index; i <= index + n; i++)
            if(options[i] == nullptr)
                return -1;
        return index;
    }
    
    // cherche une option et renvoie sa valeur ou la valeur par defaut
    const char *value( const char *name, const char *default_value )
    {
        int index= find_value(name, 1);
        if(index < 0)
            return default_value;
        
        const char *tmp= options[index+1];
        options[index]= nullptr;
        options[index+1]= nullptr;
        return tmp;
    }

    // cherche une option, renvoie un int
    int value_int( const char *name, const char *default_value )
    {
        const char *str= value(name, default_value);
        
        int v= 0;
        if(sscanf(str, "%d", &v) == 1)
            return v;
        
        // erreur parametre invalide.
        printf("invalid int parameter: %s = '%s'\n", name, str);
        
        exit(1);
        return 0;
    }
    
    // cherche une option, renvoie un unsigned long int 
    size_t value_long( const char *name, const char *default_value )
    {
        const char *str= value(name, default_value);
        
        unsigned long int v= 0;
        if(sscanf(str, "%lu", &v) == 1)
            return v;
        
        // erreur parametre invalide.
        printf("invalid int parameter: %s = '%s'\n", name, str);
        
        exit(1);
        return 0;
    }

    // cherche une option, renvoie un bool
    bool value_bool( const char *name, const char *default_value )
    {
        const char *str= value(name, default_value);
        if(std::string(str) == "true" || std::string(str) == "on" || std::string(str) == "1")
            return true;
        if(std::string(str) == "false" || std::string(str) == "off" || std::string(str) == "0")
            return false;
        
        // erreur parametre invalide.
        printf("invalid bool parameter: %s = '%s'\n", name, str);
        
        exit(1);
        return false;
    }
    
protected:
    std::vector<const char *> options = std::vector<const char *>();
};

extern Options options;

#endif
    
    
