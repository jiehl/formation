
#include <omp.h>
#include <chrono>
#include <random>
#include <stdint.h>

#include "options.h"

#include "vec.h"
#include "mat.h"

#include "color.h"
#include "framebuffer.h"
#include "image.h"
#include "image_io.h"

#include "gltf/gltf.h"
#include "gltf/scene.h"
#include "orbiter.h"

#include "sampler.h"
#include "sampler_sobol.h"

//~ typedef Squares Sampler;
typedef LCG Sampler;
//~ typedef Sobol Sampler;
//~ typedef SobolGray Sampler;
//~ typedef Owen Sampler;
//~ typedef OwenFast Sampler;
//~ typedef FCRNG Sampler;

Options options;


Vector sample_k( const float u2, const float u3 )
{
    // genere une direction ~ 1 / 2pi
    float phi= float(2*M_PI) * u2;
    float cos_theta= u3;
    float sin_theta= std::sqrt(1 - std::min(float(1.0), cos_theta*cos_theta));
    return Vector(std::cos(phi) * sin_theta, std::sin(phi) * sin_theta, cos_theta);
}

float pdf_k( const Vector&n, const Vector& v ) // global
{
    if(dot(n, v) < 0) return 0;
    return 1 / float(2*M_PI);
}

Vector sample_cos( const float u2, const float u3 )
{
    // genere une direction ~ cos theta / pi
    float phi= float(2*M_PI) * u2;
    float cos_theta= std::sqrt(u3);
    float sin_theta= std::sqrt(1 - std::min(float(1.0), cos_theta*cos_theta));
    return Vector(std::cos(phi) * sin_theta, std::sin(phi) * sin_theta, cos_theta);
}

float pdf_cos( const Vector&n, const Vector& v ) // global
{
    return std::max(float(0), dot(n, v)) / float(M_PI);
}


struct alpha_filter
{
    const Scene& scene;
    FCRNG& rng;
    
    alpha_filter( const Scene& _scene, FCRNG& _rng ) : scene(_scene), rng(_rng) {}
    
    bool operator() ( const Hit& hit ) const
    {
        const GLTFMaterial& material= scene.material(hit);
        if(material.color_texture == -1) 
            return true;
        if(scene.textures[material.color_texture].opaque)
            return true;
        
        vec3 uv_lod= scene.texcoords_lod(hit);
        Color diffuse= scene.textures[material.color_texture].sample(uv_lod.x, uv_lod.y, uv_lod.z);
        
        return (rng.sample() <= diffuse.a);
    }
};



int main( const int argc, const char **argv )
{
    options= Options(argv, argv + argc);
    
    Scene scene= read_scene(options.filename);
    if(scene.gltf.nodes.empty())
        return 1;   // pas de geometrie, pas d'image
    
    // transformations 
    Transform view;
    Transform projection;
    float aspect= 1;
    
    if(options.orbiter_filename)
    {
        Orbiter camera;
        if(camera.read_orbiter(options.orbiter_filename) == false)
            return 1;    // pas de camera, pas d'image
        
        view= camera.view();
        projection= camera.projection();
        aspect= camera.aspect();
    }
    else if(scene.gltf.cameras.size())
    {
        view= scene.camera.view;
        projection= scene.camera.projection;
        aspect= scene.camera.aspect;
    }
    else
    {
        printf("[error] no glTF camera...\n");
        return 1;   // pas de camera, pas d'image
    }
    
    // proportions largeur/hauteur de la camera gltf
    int samples= options.samples;
    int width= 1024;
    int height= width / aspect;
    Framebuffer image(width, height);
    Image aov_normal(width, height);
    Image aov_albedo(width, height);
    
    // charge les textures + parametres de filtrage en fonction de l'image
    read_textures(options.filename, scene, width, height);
    //~ read_textures(options.filename, scene, 256, 256);
    
    // 
    Transform viewport= Viewport(image.width(), image.height());
    Transform inv= Inverse(viewport * projection * view);
    
    int lines= 0;
    auto cpu_start= std::chrono::high_resolution_clock::now();
    
    // c'est parti ! parcours tous les pixels de l'image
#pragma omp parallel for schedule(dynamic, 1)
    for(int py= 0; py < image.height(); py++)
    {
    #pragma omp atomic
        lines++;
        
        if(omp_get_thread_num() == 0)
        {
            printf("%d/%d %d%%...\r", lines, image.height(), 100 * lines / image.height());
            fflush(stdout);
        }
        
        for(int px= 0; px < image.width(); px++)
        {
            const unsigned seed= (py * image.width() + px) * samples * 127;
            Sampler rng( seed );
            FCRNG alpha_rng( seed );
            
            Vector normal;
            Color albedo;
            
            for(int i= 0; i < samples; i++)
            {
                rng.index(i);
                float x= rng.sample();
                float y= rng.sample();
                
                // generer le rayon
                Point origine= inv(Point(px + x, py + y, 0));
                Point extremite= inv(Point(px + x, py + y, 1));
                Ray ray(origine, extremite);
                
                Color color;
                
                // calculer les intersections avec tous les triangles
                if(Hit hit= scene.bvh.intersect(ray, alpha_filter(scene, alpha_rng)))                
                {
                    Point p= scene.position(hit, ray);
                    Vector pn= scene.normal(hit);
                    
                    // matiere emissive, si la source est orientee vers la camera
                    if(dot(pn, ray.d) < 0)
                    {
                        const GLTFMaterial& material= scene.material(hit);
                        color= color + material.emission;
                        // suppose que les normales des sources sont correctes...
                    }
                    
                    if(dot(pn, ray.d) > 0)
                        // mais ce n'est pas toujours le cas pour le reste de la geometrie...
                        pn= -pn;
                    
                    // eclairage ambiant
                    Brdf brdf= scene.brdf(hit, pn);
                    {
                        World world(pn);
                        // genere une direction ~ cos / pi
                        Vector l= world( sample_cos(rng.sample(), rng.sample()) );
                        float pdf= pdf_cos(pn, l);
                        
                        //~ // genere une direction ~ 1/2pi
                        //~ Vector l= world( sample_k(rng.sample(), rng.sample()) );
                        //~ float pdf= pdf_k(pn, l);
                        
                        //~ // genere la direction en fonction de la matiere
                        //~ Vector l= brdf.sample(rng.sample(), rng.sample(), rng.sample(), normalize(ray.o - p));
                        //~ float pdf= brdf.pdf(l, normalize(ray.o - p));
                        
                        if(pdf == 0)
                        {
                            color= color + Black();
                            //~ color= color + Red();   // compte les echantillons qui n'ont pas reussit a generer une direction "valide"
                        }
                        else 
                            if( dot(l, Vector(0, 1, 0)) > 0 && scene.visible( offset_point(p, pn), l) )
                            {
                                // V= 1;
                                Color fr= brdf.f(l, normalize(ray.o - p));
                                float cos_theta= std::max(float(0), dot(pn, l));
                                
                                color= color + fr * cos_theta / pdf;
                                //~ color= color + Black();
                            }
                    }
                    
                    normal= normal + pn;
                    albedo= albedo + brdf.f(pn, normalize(ray.o - p));
                }
                
                image.splat(px, py, color); // accumule l'echantillon
            }
            
            aov_normal(px, py)= Color(normal / float(samples), 1);    // force une couleur opaque...
            aov_albedo(px, py)= Color(albedo / float(samples), 1);    // force une couleur opaque...
        }
    }

    auto cpu_stop= std::chrono::high_resolution_clock::now();
    auto cpu_time= std::chrono::duration_cast<std::chrono::milliseconds>(cpu_stop - cpu_start).count();
    printf("cpu  %ds %03dms\n", int(cpu_time / 1000), int(cpu_time % 1000));
    
    char tmp[1024];
    //~ sprintf(tmp, "%s-%05u-%04u.png", options.prefix, unsigned(cpu_time), samples);
    sprintf(tmp, "%s-%04u.png", options.prefix, samples);
    printf("writing '%s'...\n", tmp);
    write_image_preview(image.flatten(), tmp);
    
    //~ sprintf(tmp, "%s-%05u-%04u.pfm", options.prefix, unsigned(cpu_time), samples);
    sprintf(tmp, "%s-%04u.pfm", options.prefix, samples);
    printf("writing '%s'...\n", tmp);
    write_image_pfm(image.flatten(), tmp);
    
    sprintf(tmp, "%s-normals-%04u.pfm", options.prefix, samples);
    write_image_pfm(aov_normal, tmp);
    
    sprintf(tmp, "%s-albedo-%04u.pfm", options.prefix, samples);
    write_image_pfm(aov_albedo, tmp);
    printf("\n");
    
    return 0;
}
