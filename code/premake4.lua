dofile "gkit.lua"

 -- description des projets		 
projects = {
	"ao",
	"path",
	"path_sobol"
}

for i, name in ipairs(projects) do
    project(name)
        language "C++"
        kind "ConsoleApp"
        targetdir "bin"
        files ( gkit_files )
        files { "projects/sobol_matrices.cpp" }
        files { "projects/" .. name .. ".cpp" }
end

project("errors")
	language "C++"
	kind "ConsoleApp"
	targetdir "bin"
	files ( gkit_files )
	files { "projects/errors.cpp" }
