
#include <cstdio>
#include <cstring>
#include <cfloat>

#include "files.h"
#include "image.h"
#include "image_io.h"
#include "stb_image.h"

#include "cgltf.h"
#include "gltf.h"


static
std::vector<GLTFCamera> read_cameras( cgltf_data *data )
{
    std::vector<GLTFCamera> cameras;
    for(unsigned i= 0; i < data->nodes_count; i++)
    {
        cgltf_node *node= &data->nodes[i];
        if(node->camera == nullptr)
            continue;
        
        cgltf_camera_perspective *perspective= &node->camera->data.perspective;
        
        Transform projection= Perspective(degrees(perspective->yfov), perspective->aspect_ratio, perspective->znear, perspective->zfar);
        
        float model_matrix[16];
        cgltf_node_transform_world(node, model_matrix); // transformation globale
        
        Transform model;
        model.column_major(model_matrix);               // gltf organise les 16 floats par colonne...
        Transform view= Inverse(model);                 // view= inverse(model)
        
        Point position= model( Point(0, 0, 0) );
        Vector direction= model( Vector(0, 0, -1) );
        //~ printf("cameras[%d] position %f %f %f, direction %f %f %f\n", int(std::distance(data->cameras, node->camera)),
            //~ position.x, position.y, position.z, direction.x, direction.y, direction.z);
            
        //~ if(perspective->has_aspect_ratio)
            //~ printf("  aspect ratio %f\n", perspective->aspect_ratio);
        //~ printf("  yfov %f\n", degrees(perspective->yfov));
        //~ printf("  znear %f", perspective->znear);
        //~ if(perspective->has_zfar)
            //~ printf(", zfar %f", perspective->zfar);
        //~ printf("\n");
        
        
        cameras.push_back( { degrees(perspective->yfov), perspective->aspect_ratio, perspective->znear, perspective->zfar, view, projection } );
    }
    
    return cameras;
}

std::vector<GLTFCamera> read_gltf_cameras( const char *filename )
{
    printf("loading glTF camera '%s'...\n", filename);
    
    cgltf_options options= { };
    cgltf_data *data= nullptr;
    cgltf_result code= cgltf_parse_file(&options, filename, &data);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(cgltf_validate(data) != cgltf_result_success)
    {
        printf("[error] invalid glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(data->cameras_count == 0)
    {
        printf("[warning] no camera...\n");
        return {};
    }
    
    std::vector<GLTFCamera> cameras= read_cameras(data);
    cgltf_free(data);
    return cameras;
}


static
std::vector<GLTFLight> read_lights( cgltf_data *data )
{
    std::vector<GLTFLight> lights;
    // retrouve les transformations associees aux sources
    for(unsigned i= 0; i < data->nodes_count; i++)
    {
        cgltf_node *node= &data->nodes[i];
        if(node->light == nullptr)
            continue;
        
        // proprietes de la source 
        int light_id= int(std::distance(data->lights, node->light));
        
        cgltf_light *light= node->light;
        Color emission= Color(light->color[0], light->color[1], light->color[2]) * light->intensity;
        printf("lights[%d]: emission %f %f %f\n", light_id, emission.r, emission.g, emission.b);
        
        // position de la source
        float model_matrix[16];
        cgltf_node_transform_world(node, model_matrix); // transformation globale
        
        Transform model;
        model.column_major(model_matrix);       // gltf organise les 16 floats par colonne...
        Point position;
        Vector direction;
        
        if(light->type == cgltf_light_type_directional)
        {
            printf("  direction %f %f %f\n", direction.x, direction.y, direction.z);
            direction= model( Vector(0, 0, 1) );
        }
        if(light->type == cgltf_light_type_point)
        {
            printf("  position %f %f %f\n", position.x, position.y, position.z);
            position= model( Point(0, 0, 0) );
        }
        
        lights.push_back( { position, direction, emission } );
    }
    
    return lights;
}

std::vector<GLTFLight> read_gltf_lights( const char *filename )
{
    printf("loading glTF lights '%s'...\n", filename);
    
    cgltf_options options= { };
    cgltf_data *data= nullptr;
    cgltf_result code= cgltf_parse_file(&options, filename, &data);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(cgltf_validate(data) != cgltf_result_success)
    {
        printf("[error] invalid glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(data->lights_count == 0)
    {
        printf("[warning] no lights...\n");
        return {};
    }
    
    std::vector<GLTFLight> lights= read_lights(data);
    cgltf_free(data);
    return lights;
}


static
std::vector<GLTFMaterial> read_materials( cgltf_data *data )
{
    std::vector<GLTFMaterial> materials;
    for(unsigned i= 0; i < data->materials_count; i++)
    {
        cgltf_material *material= &data->materials[i];
        //~ printf("materials[%u] '%s'\n", i, material->name);
        
        GLTFMaterial m= { };
        m.color= Color(0.8, 0.8, 0.8, 1);
        m.emission= Color(0);
        m.metallic= 0;
        m.roughness= 1;
        m.transmission= 0;
        m.ior= 0;
        m.specular= 0;
        m.specular_color= Black();
        m.thickness= 0;
        m.attenuation_distance= 0;
        m.attenuation_color= Black();
        m.color_texture= -1;
        m.metallic_roughness_texture= -1;
        m.occlusion_texture= -1;
        m.normal_texture= -1;
        m.emission_texture= -1;
        m.transmission_texture= -1;
        m.specular_texture= -1;
        m.specular_color_texture= -1;
        m.thickness_texture= -1;
        
        if(material->has_pbr_metallic_roughness)
        {
            cgltf_pbr_metallic_roughness *pbr= &material->pbr_metallic_roughness;
            
            m.color= Color(pbr->base_color_factor[0], pbr->base_color_factor[1], pbr->base_color_factor[2], pbr->base_color_factor[3]);
            if(pbr->base_color_texture.texture && pbr->base_color_texture.texture->image)
                m.color_texture= int(std::distance(data->images, pbr->base_color_texture.texture->image));
            
            m.metallic= pbr->metallic_factor;
            m.roughness= pbr->roughness_factor;
            if(pbr->metallic_roughness_texture.texture && pbr->metallic_roughness_texture.texture->image)
                m.metallic_roughness_texture= int(std::distance(data->images, pbr->metallic_roughness_texture.texture->image));
                
            //~ printf("  pbr metallic roughness\n");
            //~ printf("    base color %f %f %f, texture %d\n", m.color.r, m.color.g, m.color.b, m.color_texture);
            //~ printf("    metallic %f, roughness %f, texture %d\n", m.metallic, m.roughness, m.metallic_roughness_texture);
        }
        //~ if(material->has_clearcoat)
            //~ printf("  clearcoat\n");
        //~ if(material->has_sheen)
            //~ printf("  sheen\n");
        
        if(material->normal_texture.texture && material->normal_texture.texture->image)
        {
            //~ printf("  normal texture %d\n", int(std::distance(data->images, material->normal_texture.texture->image)));
            m.normal_texture= int(std::distance(data->images, material->normal_texture.texture->image));
        }
        
        m.emission= Color(material->emissive_factor[0], material->emissive_factor[1], material->emissive_factor[2]);
        if(material->has_emissive_strength)
            m.emission= m.emission * material->emissive_strength.emissive_strength;
        
        if(material->emissive_texture.texture && material->emissive_texture.texture->image)
        {
            //~ printf("  emissive color %f %f %f\n", material->emissive_factor[0], material->emissive_factor[1], material->emissive_factor[2]);
            //~ printf("    texture %d\n", int(std::distance(data->images, material->emissive_texture.texture->image)));
            m.emission_texture= int(std::distance(data->images, material->emissive_texture.texture->image));
        }
        
        if(material->has_ior)
        {
            m.ior= material->ior.ior;
            if(m.ior == float(1.5))
                m.ior= 0;       // valeur par defaut
            
            //~ if(m.ior)
                //~ printf("    ior %f\n", m.ior);
        }
        
        if(material->has_specular)
        {
            m.specular= material->specular.specular_factor;
            if(material->specular.specular_texture.texture && material->specular.specular_texture.texture->image)
                m.specular_texture= std::distance(data->images, material->specular.specular_texture.texture->image);
            
            m.specular_color= Color(material->specular.specular_color_factor[0], material->specular.specular_color_factor[1], material->specular.specular_color_factor[2]);
            if(material->specular.specular_color_texture.texture && material->specular.specular_color_texture.texture->image)
                m.specular_color_texture= std::distance(data->images, material->specular.specular_color_texture.texture->image);
            
            if(m.specular_color.r*m.specular +m.specular_color.g*m.specular +m.specular_color.b*m.specular == 0)
            {
                // parametres incoherents... valeur par defaut / desactive ce comportement
                m.specular= 0;
                m.specular_color= Black();
            }
            
            //~ if(m.specular)
                //~ printf("    specular %f color %f %f %f, texture %d\n", m.specular, m.specular_color.r, m.specular_color.g, m.specular_color.b, m.specular_texture);
        }
        
        if(material->has_transmission)
        {
            m.transmission= material->transmission.transmission_factor;
            if(material->transmission.transmission_texture.texture && material->transmission.transmission_texture.texture->image)
                m.transmission_texture= std::distance(data->images, material->transmission.transmission_texture.texture->image);
            
            if(m.transmission)
                printf("    transmission %f, texture %d\n", m.transmission, m.transmission_texture);
        }
        
        if(material->has_volume)
        {
            m.thickness= material->volume.thickness_factor;
            if(material->volume.thickness_texture.texture && material->volume.thickness_texture.texture->image)
                m.thickness_texture= std::distance(data->images, material->volume.thickness_texture.texture->image);
            
            m.attenuation_distance= material->volume.attenuation_distance;
            m.attenuation_color= Color(material->volume.attenuation_color[0], material->volume.attenuation_color[1], material->volume.attenuation_color[2]);
            //~ printf("    volume thickness %f, texture %d\n",m.thickness, m.thickness_texture);
            //~ printf("    volume attenation distance %f, color %f %f %f\n", m.attenuation_distance, m.attenuation_color.r, m.attenuation_color.g, m.attenuation_color.b);
        }
        
        materials.push_back(m);
    }
    
    return materials;
}

std::vector<GLTFMaterial> read_gltf_materials( const char *filename )
{
    printf("loading glTF materials '%s'...\n", filename);
    
    cgltf_options options= { };
    cgltf_data *data= nullptr;
    cgltf_result code= cgltf_parse_file(&options, filename, &data);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(cgltf_validate(data) != cgltf_result_success)
    {
        printf("[error] invalid glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(data->materials_count ==0)
    {
        printf("[warning] no materials...\n");
        return {};
    }
    
    std::vector<GLTFMaterial> materials= read_materials(data);
    cgltf_free(data);
    return materials;
}


std::vector<Image> read_gltf_images( const char *filename, const int width_max, const int height_max )
{
    printf("loading glTF images '%s'...\n", filename);
    
    cgltf_options options= { };
    cgltf_data *data= nullptr;
    cgltf_result code= cgltf_parse_file(&options, filename, &data);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(cgltf_validate(data) != cgltf_result_success)
    {
        printf("[error] invalid glTF mesh '%s'...\n", filename);
        return {};
    }
    
    if(data->images_count == 0)
        return {};
    
    // detecte s'il faut charger aussi les buffers... textures internes / .glb
    for(unsigned i= 0; i < data->images_count; i++)
        if(!data->images[i].uri)
        {
            code= cgltf_load_buffers(&options, data, filename);
            if(code != cgltf_result_success)
            {
                printf("[error] loading glTF internal images...\n");
                cgltf_free(data);
                return {};
            }
            
            break;
        }
    
    // marque les textures a convertir srgb -> rgb
    std::vector<int> convert(data->images_count, 0);
    for(unsigned i= 0; i < data->materials_count; i++)
    {
        cgltf_material *material= &data->materials[i];
        if(material->has_pbr_metallic_roughness)
        {
            cgltf_pbr_metallic_roughness *pbr= &material->pbr_metallic_roughness;
            
            if(pbr->base_color_texture.texture && pbr->base_color_texture.texture->image)
            {
                int texture_id= int(std::distance(data->images, pbr->base_color_texture.texture->image));
                convert[texture_id]= 1;
            }
            // les autres textures ne sont pas des couleurs...
        }
    }
    
    // charge toutes les images en parallele
    std::vector<Image> images(data->images_count);
    
#pragma omp parallel for schedule(dynamic, 1)
    for(unsigned i= 0; i < data->images_count; i++)
    {
        if(data->images[i].uri)
        {
            // charge le fichier
            //~ printf("  [%u] %s\n", i, data->images[i].uri);
            std::string image_filename= pathname(filename) + std::string(data->images[i].uri);
            
            stbi_ldr_to_hdr_scale(1.0);
            stbi_ldr_to_hdr_gamma(1.0);
            if(convert[i])
                stbi_ldr_to_hdr_gamma(2.2);
            
            int width, height, channels;
            float *tmp= stbi_loadf(image_filename.c_str(), &width, &height, &channels, 4);
            assert(tmp);
            
            // \todo : remplacer le vector d'image par un pointeur et virer la copie...
            // ou en profiter pour utiliser les conversion srgb -> rgb plus precises que gamma(2.2)...
            images[i]= Image(width, height);
            {
                Image& image= images[i];
                for(unsigned p= 0, offset= 0; p < image.size(); p++, offset+= 4)
                {
                    Color pixel= Color( 
                        tmp[offset], 
                        tmp[offset + 1],
                        tmp[offset + 2],
                        tmp[offset + 3]);
                    image(p)= pixel;
                }
            }
            
            stbi_image_free(tmp);
        }
        else if(data->images[i].buffer_view)
        {
            // extraire l'image du glb...
            cgltf_buffer_view *view= data->images[i].buffer_view;
            assert(view->buffer->data);
            //~ printf("  [%u] %s offset %lu\n", i, data->images[i].name, view->offset);
            //~ printf("  [%u] %s\n", i, data->images[i].name);
            
            stbi_ldr_to_hdr_scale(1.0);
            stbi_ldr_to_hdr_gamma(1.0);
            if(convert[i])
                stbi_ldr_to_hdr_gamma(2.2);
            
            int width, height, channels;
            float *tmp= stbi_loadf_from_memory((const unsigned char *) view->buffer->data + view->offset, view->size, &width, &height, &channels, 4);
            assert(tmp);
          
            //~ printf("  images[%u] '%s' %dx%dx%d...\n", i, data->images[i].name, width, height, channels);
            // \todo : remplacer le vector d'image par un pointeur et virer la copie...
            images[i]= Image(width, height);
            {
                Image& image= images[i];
                for(unsigned p= 0, offset= 0; p < image.size(); p++, offset+= 4)
                {
                    Color pixel= Color( 
                        tmp[offset], 
                        tmp[offset + 1],
                        tmp[offset + 2],
                        tmp[offset + 3]);
                    image(p)= pixel;
                }
            }
            
            stbi_image_free(tmp);
        }
        
        // reduit la taille des textures, si necessaire...
        if(width_max && height_max)
        {
            while(images[i].width() > width_max * 2 && images[i].height() > height_max * 2)
                images[i]= downscale(images[i]);
        }
    }
    
    cgltf_free(data);
    return images;
}


GLTFScene read_gltf_scene( const char *filename )
{
    printf("loading glTF scene '%s'...\n", filename);
    
    cgltf_options options= { };
    cgltf_data *data= nullptr;
    cgltf_result code= cgltf_parse_file(&options, filename, &data);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF mesh '%s'...\n", filename);
        return { };
    }
    
    if(cgltf_validate(data) != cgltf_result_success)
    {
        printf("[error] invalid glTF mesh '%s'...\n", filename);
        return { };
    }
    
    code= cgltf_load_buffers(&options, data, filename);
    if(code != cgltf_result_success)
    {
        printf("[error] loading glTF buffers...\n");
        return { };
    }
    
    //
    GLTFScene scene;
    
// etape 1 : construire les meshs et les groupes de triangles / primitives
    int primitives_index= 0;
    std::vector<float> buffer;
    
    // parcourir tous les meshs de la scene
    for(unsigned mesh_id= 0; mesh_id < data->meshes_count; mesh_id++)
    {
        GLTFMesh m= { };
        m.pmin= Point(FLT_MAX, FLT_MAX, FLT_MAX);
        m.pmax= Point(-FLT_MAX, -FLT_MAX, -FLT_MAX);
        
        cgltf_mesh *mesh= &data->meshes[mesh_id];
        // parcourir les groupes de triangles du mesh...
        for(unsigned primitive_id= 0; primitive_id < mesh->primitives_count; primitive_id++)
        {
            cgltf_primitive *primitives= &mesh->primitives[primitive_id];
            assert(primitives->type == cgltf_primitive_type_triangles);
            
            GLTFPrimitives p= { };
            
            // matiere associee au groupe de triangles
            p.material_index= -1;
            if(primitives->material)
                p.material_index= std::distance(data->materials, primitives->material);
            
            // indices
            if(primitives->indices)
            {
                for(unsigned i= 0; i < primitives->indices->count; i++)
                    p.indices.push_back(cgltf_accessor_read_index(primitives->indices, i));
                assert(p.indices.size() % 3 == 0);
            }
            
            // attributs
            for(unsigned attribute_id= 0; attribute_id < primitives->attributes_count; attribute_id++)
            {
                cgltf_attribute *attribute= &primitives->attributes[attribute_id];
                
                if(attribute->type == cgltf_attribute_type_position)
                {
                    assert(attribute->data->type == cgltf_type_vec3);
                    
                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());
                    
                    // transforme les positions des sommets
                    for(unsigned i= 0; i+2 < buffer.size(); i+= 3)
                        p.positions.push_back( vec3(buffer[i], buffer[i+1], buffer[i+2]) );
                    
                #if 1
                    // englobant des primitives
                    assert(attribute->data->has_min);
                    assert(attribute->data->has_max);
                    p.pmin= Point(attribute->data->min[0], attribute->data->min[1], attribute->data->min[2]);
                    p.pmax= Point(attribute->data->max[0], attribute->data->max[1], attribute->data->max[2]);
                #else
                    p.pmin= p.positions[0];
                    p.pmax= p.positions[0];
                    for(unsigned i= 1; i < p.positions.size(); i++)
                    {
                        p.pmin= min(p.pmin, p.positions[i]);
                        p.pmax= max(p.pmax, p.positions[i]);
                    }
                #endif
                    
                    // englobant du mesh
                    m.pmin= min(m.pmin, p.pmin);
                    m.pmax= max(m.pmax, p.pmax);
                }
                
                if(attribute->type == cgltf_attribute_type_normal)
                {
                    assert(attribute->data->type == cgltf_type_vec3);
                    
                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());
                    
                    // transforme les normales des sommets
                    for(unsigned i= 0; i+2 < buffer.size(); i+= 3)
                        p.normals.push_back( vec3(buffer[i], buffer[i+1], buffer[i+2]) );
                }
                
                if(attribute->type == cgltf_attribute_type_texcoord)
                {
                    assert(attribute->data->type == cgltf_type_vec2);
                    
                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());
                    
                    for(unsigned i= 0; i+1 < buffer.size(); i+= 2)
                        p.texcoords.push_back( vec2(buffer[i], buffer[i+1]) );
                }
            }
            
            p.primitives_index= primitives_index++;
            m.primitives.push_back(p);
        }
        
        scene.meshes.push_back(m);
    }
    
// etape 2 : parcourir les noeuds, retrouver les transforms pour placer les meshes
    for(unsigned node_id= 0; node_id < data->nodes_count; node_id++)
    {
        cgltf_node *node= &data->nodes[node_id];
        if(node->mesh== nullptr)
            // pas de mesh associe, rien a dessiner
            continue;
        
        // recuperer la transformation pour placer le mesh dans la scene
        float model_matrix[16];
        cgltf_node_transform_world(node, model_matrix);
        
        Transform model;
        model.column_major(model_matrix);       // gltf organise les 16 floats par colonne...
        Transform normal= model.normal();       // transformation pour les normales
        
        int mesh_index= std::distance(data->meshes, node->mesh);
        scene.nodes.push_back( {model, normal, mesh_index} );
    }
    
// etape 3 : recuperer les autres infos...
    scene.materials= read_materials(data);
    scene.lights= read_lights(data);
    scene.cameras= read_cameras(data);
    
// etape : nettoyage...
    cgltf_free(data);
    
    return scene;
}

std::vector<GLTFInstances> GLTFScene::instances( ) const
{
    std::vector<GLTFInstances> instances(meshes.size());
    for(unsigned i= 0; i < meshes.size(); i++)
        instances[i].mesh_index= i;
    
    for(unsigned i= 0; i < nodes.size(); i++)
    {
        int index= nodes[i].mesh_index;
        assert(index < int(instances.size()));
        instances[index].transforms.push_back( nodes[i].model );
    }
    
    return instances;
}


void GLTFScene::bounds( Point& pmin, Point& pmax ) const
{
    pmin= Point(FLT_MAX, FLT_MAX, FLT_MAX);
    pmax= Point(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    
    for(unsigned node_id= 0; node_id < nodes.size(); node_id++)
    {
        const GLTFNode& node= nodes[node_id];
        const GLTFMesh& mesh= meshes[node.mesh_index];
        
        // transforme la bbox du mesh dans le repere la scene
        for(int i= 0; i < 8; i++)
        {
            Point p= mesh.pmin;
            if(i & 1) p.x= mesh.pmax.x;
            if(i & 2) p.y= mesh.pmax.y;
            if(i & 4) p.z= mesh.pmax.z;
            
            p= node.model(p);
            pmin= min(pmin, p);
            pmax= max(pmax, p);
        }
    }
}
