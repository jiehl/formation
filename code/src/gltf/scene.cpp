
#include "gltf/scene.h"


Scene read_scene( const char *filename )
{
    Scene scene;
    scene.gltf= read_gltf_scene(filename);
    
    // camera
    if(scene.gltf.cameras.empty())
        return {};  // pas de camera, pas d'image
    scene.camera= scene.gltf.cameras[0];

    // extrait les triangles 
    if(scene.gltf.nodes.empty())
        return {};  // pas de triangles, pas d'image
    
    std::vector<Triangle> triangles;
    std::vector<Source> sources;
    for(unsigned node_id= 0; node_id < scene.gltf.nodes.size(); node_id++)
    {
        const GLTFNode& node= scene.gltf.nodes[node_id];
        
        const Transform& model= node.model;
        int mesh_id= node.mesh_index;
        
        const GLTFMesh& mesh= scene.gltf.meshes[mesh_id];
        for(unsigned primitive_id= 0; primitive_id < mesh.primitives.size(); primitive_id++)
        {
            const GLTFPrimitives& primitives= mesh.primitives[primitive_id];
            
            int material_id= primitives.material_index;
            assert(material_id != -1);
            const GLTFMaterial& material= scene.gltf.materials[material_id];
            bool emissive= (material.emission.max() > 0);
            
            for(unsigned i= 0; i +2 < primitives.indices.size(); i+= 3)
            {
                // indice des sommets
                int a= primitives.indices[i];
                int b= primitives.indices[i+1];
                int c= primitives.indices[i+2];
                
                // transforme les sommets dans le repere de la scene
                Point pa= model( Point(primitives.positions[a]) );
                Point pb= model( Point(primitives.positions[b]) );
                Point pc= model( Point(primitives.positions[c]) );
                
                // verifie que le triangle n'est pas degenere
                float area= length( cross( Vector(pa, pb), Vector(pa, pc) ) );
                if(area > 0)
                {
                    triangles.push_back( Triangle(pa, pb, pc, node_id, mesh_id, primitive_id, i/3) );
                    
                    // construit aussi les sources / triangles emissifs
                    if(emissive)
                        sources.push_back( Source(pa, pb, pc, material.emission) );
                }
            }
        }
    }
    assert(triangles.size());
    
    // et construit le bvh
    scene.bvh.build(triangles);
    // et la cdf des sources
    scene.sources.build(sources);
    
    return scene;
}


int read_textures( const char *filename, Scene& scene, const int width, const int height )
{
    std::vector<Image> images= read_gltf_images(filename, width, height);
    
    scene.textures.resize(images.size());
    
#pragma omp parallel for schedule(dynamic, 1)
    for(unsigned i=0; i < images.size(); i++)
    {
        scene.textures[i]= Texture(images[i]);
        images[i]= Image();  // nettoyage...
    }
    
    // filtrage des textures
    scene.image_width= width;
    scene.image_height= height;
    scene.pixel_cone_angle= std::atan( std::tan(radians(scene.camera.fov)) / scene.image_height );
    scene.view= scene.camera.view;  // transformation dans le repere camera pour evaluer la distance / lod
    
    printf("%d textures...\n", int(scene.textures.size()));
    return int(scene.textures.size());
}

const GLTFMaterial& Scene::material( const Hit& hit ) const 
{
    assert(bool(hit));
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    int id= primitives.material_index;
    assert(id != -1);
    return gltf.materials[id];
}

float Scene::triangle_area( const Hit& hit ) const
{
    assert(bool(hit));
    const GLTFNode& node= gltf.nodes[hit.node_id];
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    // indice des sommets
    int first= 3*hit.triangle_id;
    int a= primitives.indices[first];
    int b= primitives.indices[first+1];
    int c= primitives.indices[first+2];
    
    // positions des sommets
    assert(primitives.positions.size());
    Point pa= node.model( Point(primitives.positions[a]) );
    Point pb= node.model( Point(primitives.positions[b]) );
    Point pc= node.model( Point(primitives.positions[c]) );
    Vector ng= cross( Vector(pa, pb), Vector(pa, pc) );
    return length(ng) / 2;
}


Brdf Scene::brdf( const Hit& hit, const Vector& n ) const
{
    assert(bool(hit));
    
    const GLTFMaterial& m= material(hit);
    Color color= m.color;
    float roughness= m.roughness;
    float metallic= m.metallic;
    
    // ajuste les parametres en fonction des textures...
    if(m.color_texture != -1 || m.metallic_roughness_texture != -1)
    {
        vec3 uv_lod= texcoords_lod(hit);
        if(m.color_texture != -1)
            color= color * textures[m.color_texture].sample(uv_lod.x, uv_lod.y, uv_lod.z);
        
        if(m.metallic_roughness_texture != -1)
        {
            Color pixel= textures[m.metallic_roughness_texture].sample(uv_lod.x, uv_lod.y, uv_lod.z);
            metallic= metallic * pixel.b;
            roughness= roughness * pixel.g;
        }
    }
    
    return Brdf(n, color, metallic, roughness);
}


// renvoie la normale interpolee au point d'intersection
Point Scene::position( const Hit& hit ) const 
{
    assert(bool(hit));
    const GLTFNode& node= gltf.nodes[hit.node_id];
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    // indice des sommets
    int first= 3*hit.triangle_id;
    int a= primitives.indices[first];
    int b= primitives.indices[first+1];
    int c= primitives.indices[first+2];
    
    // positions des sommets
    assert(primitives.positions.size());
    Point pa= primitives.positions[a];
    Point pb= primitives.positions[b];
    Point pc= primitives.positions[c];
    
    // interpole au point d'intersection
    Point p= (1 - hit.u - hit.v) * pa + hit.u * pb + hit.v * pc;
    
    // transforme dans le repere de la scene
    return node.model(p);
}

// renvoie les coordonnees de texture interpolee au point d'intersection
vec2 Scene::texcoords( const Hit& hit ) const
{
    assert(bool(hit));
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    // indice des sommets
    int first= 3*hit.triangle_id;
    int a= primitives.indices[first];
    int b= primitives.indices[first+1];
    int c= primitives.indices[first+2];

    // texcoords des sommets
    assert(primitives.texcoords.size());
    vec2 ta= primitives.texcoords[a];
    vec2 tb= primitives.texcoords[b];
    vec2 tc= primitives.texcoords[c];
    
    // interpole au point d'intersection
    float tx= (1 - hit.u - hit.v) * ta.x + hit.u * tb.x + hit.v * tc.x;
    float ty= (1 - hit.u - hit.v) * ta.y + hit.u * tb.y + hit.v * tc.y;
    return vec2(tx, ty);
}


// renvoie les coordonnees de texture interpolee au point d'intersection + lod
vec3 Scene::texcoords_lod( const Hit& hit ) const
{
    assert(bool(hit));
    const GLTFNode& node= gltf.nodes[hit.node_id];    
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    // indice des sommets
    int first= 3*hit.triangle_id;
    int a= primitives.indices[first];
    int b= primitives.indices[first+1];
    int c= primitives.indices[first+2];

    // texcoords
    assert(primitives.texcoords.size());
    vec2 ta= primitives.texcoords[a];
    vec2 tb= primitives.texcoords[b];
    vec2 tc= primitives.texcoords[c];
    
    float tx= (1 - hit.u - hit.v) * ta.x + hit.u * tb.x + hit.v * tc.x;
    float ty= (1 - hit.u - hit.v) * ta.y + hit.u * tb.y + hit.v * tc.y;
    
    // positions 
    assert(primitives.positions.size());
    Point pa= Point(primitives.positions[a]);
    Point pb= Point(primitives.positions[b]);
    Point pc= Point(primitives.positions[c]);
    Point p= (1 - hit.u - hit.v) * pa + hit.u * pb + hit.v * pc;
    
    Transform mv= view * node.model;
    Vector d= Vector( mv(p) );
    
    // estime le filtrage des textures
    //   "Texture Level of Detail Strategies for Real-Time Ray Tracing", ray tracing gems 1, chapitre 20
    //   https://media.contentapi.ea.com/content/dam/ea/seed/presentations/2019-ray-tracing-gems-chapter-20-akenine-moller-et-al.pdf
    float texarea= length( cross( Vector(Point(ta, 0), Point(tb, 0)), Vector(Point(ta, 0), Point(tc, 0)) ));
    float area= length( cross( Vector(pa, pb), Vector(pa, pc) ) );
    float lod= std::log2( std::sqrt(texarea/area) * pixel_cone_angle * length(d) );
    // todo stocker area dans les triangles du bvh ??
    
    return vec3(tx, ty, lod);
    //~ return vec3(tx, ty, 0);
}


// renvoie la normale interpolee au point d'intersection
Vector Scene::normal( const Hit& hit ) const 
{
    assert(bool(hit));
    const GLTFNode& node= gltf.nodes[hit.node_id];
    const GLTFMesh& mesh= gltf.meshes[hit.mesh_id];
    const GLTFPrimitives& primitives= mesh.primitives[hit.primitive_id];
    
    // indice des sommets
    int first= 3*hit.triangle_id;
    int a= primitives.indices[first];
    int b= primitives.indices[first+1];
    int c= primitives.indices[first+2];
    
    // normales des sommets
    assert(primitives.normals.size());
    Vector na= primitives.normals[a];
    Vector nb= primitives.normals[b];
    Vector nc= primitives.normals[c];
    
    // interpole la normale au point d'intersection
    Vector n= (1 - hit.u - hit.v) * na + hit.u * nb + hit.v * nc;
    
    // transforme la normale dans le repere de la scene
    return normalize( node.normal(n) );
}

//~ Source& Scene::source( const Hit& hit );   // todo

