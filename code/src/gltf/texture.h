
#pragma once

#include <vector>

#include "image.h"
#include "image_io.h"


struct Texture
{
    std::vector<Image> mips;
    bool opaque;
    
    Texture( ) : mips(), opaque(true) {}
    
    Texture( const Image& mip0 ) : mips(), opaque(true)
    {
        assert(mip0.size());
        mips.push_back(mip0);
        
        // verifie l'opacite de la texture...
        for(unsigned i= 0; i < mip0.size(); i++)
            if(mip0(i).a < float(0.5))
            {
                opaque= false;
                break;
            }
        
        Image mip= mip0;
        while(mip.width() / 2 > 1 || mip.height() / 2 > 1)
        {
            mip= downscale(mip);
            mips.push_back(mip);
        }
    }
    
    Color sample( const float u, const float v ) const
    {
        return sample_repeat(u, v, 0);
    }
    
    Color sample( const float u, const float v, const float lod ) const
    {
        return sample_repeat(u, v, lod);        // par defaut
    }   
    
    // adressage des textures lorsque les coordonnees ne sont pas entre 0 et 1
    // https://registry.khronos.org/OpenGL/specs/gl/glspec46.core.pdf#subsection.8.14.2
    int clamp( const int x, const int size ) const
    {
        if(x < 0) return 0;
        if(x > size) return size;
        return x;
    }
    
#if 0
    // % n'est pas modulo...
    int mod( const int x, const int size ) const
    {
        return (x % size + size) % size;
    }
    
    int nearest( const int x, const int size ) const
    {
        return x;
    }
    
    int repeat( const int x, const int size ) const
    {
        return mod(x, size);
    }
    
    int mirror( const int x ) const
    {
        if(x >= 0) return x;
        return -(1 + x);
    }
    
    int mirror_repeat( const int x, const int size ) const
    {
        return (size - 1) + mirror(mod(x, 2*size) - size);
    }
#else
    float mod( const float x, const int size ) const
    {
        return std::fmod(std::abs(x), float(size));
    }
    
    float nearest( const float x, const int size ) const
    {
        return std::floor(std::abs(x));
    }
    
    float repeat( const float x, const int size ) const
    {
        return mod(x, size);
    }
    
    float mirror( const float x ) const
    {
        if(x >= 0) return x;
        return -(1 + x);
    }
    
    float mirror_repeat( const float x, const int size ) const
    {
        return mod(x, size);
    }
    
#endif

    // cf GL_NEAREST
    Color sample_nearest( const float u, const float v, const float lod ) const
    {
        int level= clamp(std::floor(lod), mips.size() -1);
        int w= mips[level].width();
        int h= mips[level].height();
        return mips[level].sample(nearest(u * w, w), nearest(v * h, h));
    }
    
    // cf GL_REPEAT
    Color sample_repeat( const float u, const float v, const float lod ) const
    {
        int level= clamp(std::floor(lod), mips.size() -1);
        int w= mips[level].width();
        int h= mips[level].height();
        return mips[level].sample(repeat(u * w, w), repeat(v * h, h));
    }
    
    //~ // cf GL_MIRRORED_REPEAT
    //~ Color sample_mirror_repeat( const float u, const float v, const float lod ) const
    //~ {
        //~ int level= clamp(std::floor(lod), mips.size() -1);
        //~ int w= mips[level].width();
        //~ int h= mips[level].height();
        //~ return mips[level].sample(mirror_repeat(u * w, w), mirror_repeat(v * h, h));
    //~ }
    
    // cf GL_CLAMP_TO_XXX
    Color sample_clamp( const float u, const float v, const float lod ) const
    {
        int level= clamp(std::floor(lod), mips.size() -1);
        int w= mips[level].width();
        int h= mips[level].height();
        return mips[level].sample(clamp(u * w, w), clamp(v * h, h));
    }
};
