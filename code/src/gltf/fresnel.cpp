
#include <cassert>
#include <cmath>

#include "fresnel.h"


Vector reflect( const Vector& w, const Vector& n )
{
    return 2*dot(w, n) * n - w;
}

bool can_refract( const float ni, const float nt, const Vector& w, const Vector& n ) 
{
    float cos_theta= dot(n, w);
    // cf http://www.pbr-book.org/3ed-2018/Reflection_Models/Specular_Reflection_and_Transmission.html
    float sin2_theta= std::max(float(0), 1 - cos_theta*cos_theta);
    float inv_eta= ni / nt;
    float sin2_theta_t= inv_eta*inv_eta * sin2_theta;
    return sin2_theta_t < 1;
}

Vector refract( const float ni, const float nt, const Vector& w, const Vector& n )
{
    float cos_theta= dot(n, w);
    // cf http://www.pbr-book.org/3ed-2018/Reflection_Models/Specular_Reflection_and_Transmission.html
    float sin2_theta= std::max(float(0), 1 - cos_theta*cos_theta);
    float inv_eta= ni / nt;
    float sin2_theta_t= inv_eta*inv_eta * sin2_theta;
    assert(sin2_theta_t <= 1);
    float cos_theta_t= std::sqrt(std::max(float(0), 1 - sin2_theta_t));
    return -inv_eta * w + (inv_eta * cos_theta - cos_theta_t) * n;
}


static 
float sqr( const float x ) { return x*x; }

float fresnel( const float ni, const float nt, const Vector& w, const Vector& n )
{
    float eta= nt / ni;
    float c= std::abs(dot(n, w));
    float g= eta*eta + c*c -1;
    if(g < 0)
        return 1; // reflexion totale
    
    g= std::sqrt(g);
    return sqr( g-c ) / sqr( g+c ) / 2 * ( 1 + sqr( c*(g+c) -1 ) / sqr( c*(g-c)+1 ) );
}

Color schlick_fresnel( const Color& F0, const float cos_theta )
{
    return F0 + (Color(1) - F0) * std::pow(std::max(float(0), 1 - cos_theta), float(5));
}

Color schlick_fresnel( const float ni, const float nt, const Vector& w, const Vector& n )
{
    // dot(n, reflect)
    float cos_theta= dot(n, w);

    if(ni > nt)
    {
        // re-evalue l'approximation lorsque ni > nt...
        float sin2_theta= std::max(float(0), 1 - cos_theta*cos_theta);
        float inv_eta= ni / nt;
        float sin2_theta_t= inv_eta*inv_eta * sin2_theta;
        if(sin2_theta_t > 1)
        // reflexion totale...
        return White();
        
        // dot(n, refract)
        cos_theta= std::sqrt(std::max(float(0), 1 - sin2_theta_t));
    }

    return schlick_fresnel( Color(schlick_f0(ni, nt)), cos_theta);
}

float schlick_f0( const float ior )
{
    float f= (ior -1) / (ior +1);
    return f*f;
}

float schlick_f0( const float ni, const float nt )
{
    float f= (nt - ni) / (nt + ni);
    return f*f;
}

float schlick_ior( const float f0 )
{
    float f= std::sqrt(f0);
    return (1 + f ) / (1 - f);
}

