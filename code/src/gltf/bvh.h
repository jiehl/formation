
#pragma once

#include <cfloat>
#include <climits>
#include <chrono>
#include <algorithm>

#include "gltf.h"


// intersection avec un triangle
struct Hit
{
    float t;            //!< p(t)= o + td, position du point d'intersection sur le rayon
    float u, v;         //!< p(u, v), position du point d'intersection sur le triangle
    int node_id;        //!< indexation dans une scene gltf
    int mesh_id;
    int primitive_id;
    int triangle_id;
    
    Hit( ) : t(FLT_MAX), u(), v(), node_id(-1), mesh_id(-1), primitive_id(-1), triangle_id(-1) {}
    
    Hit( const float _t, const float _u, const float _v, const int _node_id, const int _mesh_id, const int _primitive_id, const int _id ) : t(_t), u(_u), v(_v), 
        node_id(_node_id), mesh_id(_mesh_id), primitive_id(_primitive_id), triangle_id(_id) {}
    
    operator bool ( ) const { return (triangle_id != -1); }   // renvoie vrai si l'intersection est definie / existe
};


// intersection avec une boite englobante
struct BBoxHit
{
    float tmin, tmax;
    
    BBoxHit() : tmin(FLT_MAX), tmax(-FLT_MAX) {}
    BBoxHit( const float _tmin, const float _tmax ) : tmin(_tmin), tmax(_tmax) {}
    
    operator bool( ) const { return tmin <= tmax; }   // renvoie vrai si l'intersection est definie / existe
};


// rayon 
struct Ray
{
    Point o;            //!< origine
    Vector d;           //!< direction
    float tmax;         //!< tmax= 1 ou \inf, le rayon est un segment ou une demi droite infinie
    
    Ray( const Point& _o, const Point& _e ) : o(_o), d(Vector(_o, _e)), tmax(1) {} // segment, t entre 0 et 1
    Ray( const Point& _o, const Vector& _d ) : o(_o), d(_d), tmax(FLT_MAX) {}  // demi droite, t entre 0 et \inf
    Ray( const Point& _o, const Vector& _d, const float _tmax ) :  o(_o), d(_d), tmax(_tmax) {} // explicite
    
    Point operator() ( const Hit& hit ) const { assert(bool(hit)); return o + hit.t * d; }
};


// boite englobante
struct BBox
{
    Point pmin, pmax;
    
    BBox( ) : pmin(), pmax() {}
    
    explicit BBox( const Point& p ) : pmin(p), pmax(p) {}
    BBox( const Point& a, const Point& b ) : pmin(a), pmax(b) {}
    
    BBox( const BBox& box ) : pmin(box.pmin), pmax(box.pmax) {}
    BBox( const BBox& a, const BBox& b ) : pmin(min(a.pmin, b.pmin)), pmax(max(a.pmax, b.pmax)) {}
    
    BBox& operator= ( const BBox& box ) { pmin= box.pmin; pmax= box.pmax; return *this; }
    
    BBox& insert( const Point& p ) { pmin= min(pmin, p); pmax= max(pmax, p); return *this; }
    BBox& insert( const BBox& box ) { pmin= min(pmin, box.pmin); pmax= max(pmax, box.pmax); return *this; }
    
    float centroid( const int axis ) const { return (pmin(axis) + pmax(axis)) / 2; }
    Point centroid( ) const { return (pmin + pmax) / 2; }
    
    BBoxHit intersect( const Ray& ray, const Vector& invd, const float htmax ) const
    {
        Point rmin= pmin;
        Point rmax= pmax;
        if(ray.d.x < 0) std::swap(rmin.x, rmax.x);
        if(ray.d.y < 0) std::swap(rmin.y, rmax.y);
        if(ray.d.z < 0) std::swap(rmin.z, rmax.z);
        Vector dmin= (rmin - ray.o) * invd;
        Vector dmax= (rmax - ray.o) * invd;
        
        float tmin= std::max(dmin.z, std::max(dmin.y, std::max(dmin.x, 0.f)));
        float tmax= std::min(dmax.z, std::min(dmax.y, std::min(dmax.x, htmax)));
        return BBoxHit(tmin, tmax);
    }

    bool empty( ) const { return (pmin.x >= pmax.x && pmin.y >= pmax.y && pmin.z >= pmax.z); }
    
    float area( ) const
    {
        if(empty()) return 0;
        
        Vector d(pmin, pmax);
        return 2 * (d.x*d.y + d.x*d.z + d.y*d.z);
    }    
};

static inline  
BBox EmptyBox( ) { return BBox( Point(FLT_MAX, FLT_MAX, FLT_MAX), Point(-FLT_MAX, -FLT_MAX, -FLT_MAX) ); }



// triangle pour le bvh, cf fonction bounds() et intersect()
struct Triangle
{
    Point p;            //!< sommet a du triangle
    Vector e1, e2;      //!< aretes ab, ac du triangle
    int node_id;        //!< indexation dans une scene gltf
    int mesh_id;
    int primitive_id;
    int triangle_id;
    
    Triangle() {}
    
    Triangle( const vec3& a, const vec3& b, const vec3& c, const int _node_id, const int _mesh_id, const int _primitive_id, const int _id ) : 
        p(a), e1(Vector(a, b)), e2(Vector(a, c)), 
        node_id(_node_id), mesh_id(_mesh_id), primitive_id(_primitive_id), triangle_id(_id) {}
    
    /* calcule l'intersection ray/triangle
        cf "fast, minimum storage ray-triangle intersection" 
        
        renvoie faux s'il n'y a pas d'intersection valide (une intersection peut exister mais peut ne pas se trouver dans l'intervalle [0 tmax] du rayon.)
        renvoie vrai + les coordonnees barycentriques (u, v) du point d'intersection + sa position le long du rayon (t).
        convention barycentrique : p(u, v)= (1 - u - v) * a + u * b + v * c
    */
    Hit intersect( const Ray &ray, const float htmax ) const
    {
        Vector pvec= cross(ray.d, e2);
        float det= dot(e1, pvec);
        
        float inv_det= 1 / det;
        Vector tvec(p, ray.o);
        
        float u= dot(tvec, pvec) * inv_det;
        if(u < 0 || u > 1) return Hit();
        
        Vector qvec= cross(tvec, e1);
        float v= dot(ray.d, qvec) * inv_det;
        if(v < 0 || u + v > 1) return Hit();
        
        float t= dot(e2, qvec) * inv_det;
        if(t < 0 || t > htmax) return Hit();
        
        return Hit(t, u, v, node_id, mesh_id, primitive_id, triangle_id);
    }
    
    BBox bounds( ) const 
    {
        BBox box(p);
        return box.insert(p+e1).insert(p+e2);
    }
};


// construction de l'arbre / BVH
struct Node
{
    BBox bounds;
    int left;
    int right;
    
    bool internal( ) const { return right > 0; }                        // renvoie vrai si le noeud est un noeud interne
    int internal_left( ) const { assert(internal()); return left; }     // renvoie le fils gauche du noeud interne 
    int internal_right( ) const { assert(internal()); return right; }   // renvoie le fils droit
    
    bool leaf( ) const { return right < 0; }                            // renvoie vrai si le noeud est une feuille
    int leaf_begin( ) const { assert(leaf()); return -left; }           // renvoie le premier objet de la feuille
    int leaf_end( ) const { assert(leaf()); return -right; }            // renvoie le dernier objet
};

// creation d'un noeud interne
static inline
Node make_node( const BBox& bounds, const int left, const int right )
{
    Node node { bounds, left, right };
    assert(node.internal());    // verifie que c'est bien un noeud...
    return node;
}

// creation d'une feuille
static inline
Node make_leaf( const BBox& bounds, const int begin, const int end )
{
    Node node { bounds, -begin, -end };
    assert(node.leaf());        // verifie que c'est bien une feuille...
    return node;
}


struct BVH
{
    std::vector<Node> nodes;
    std::vector<Triangle> triangles;
    int root;
    
    struct Primitive
    {
        BBox bounds;
        Point centroid;
        unsigned id;
    };
    std::vector<Primitive> primitives;
    
    int nodes_count;
    
    // construit un bvh pour l'ensemble de triangles
    int build( const std::vector<Triangle>& _triangles )
    {
        primitives.clear();
        triangles.clear();
        primitives.reserve(_triangles.size());
        triangles.reserve(_triangles.size());
        
        nodes.resize(_triangles.size()*2);
        root= 0;
        nodes_count= 2;
        
        for(unsigned i= 0; i < _triangles.size(); i++)
        {
            BBox bounds= _triangles[i].bounds();
            primitives.push_back( { bounds, bounds.centroid(), i } );
        }
        
        auto start= std::chrono::high_resolution_clock::now();
        {
            //~ build(root, 0, primitives.size());   // decoupe l'axe le plus long de l'englobant
            build_bins(root, 0, primitives.size());  // binned sah
        }
        auto stop= std::chrono::high_resolution_clock::now();
        int cpu= std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
        printf("BVH %dms, %u triangles\n", cpu, unsigned(primitives.size()));
        
        // re-organise les triangles
        for(unsigned i= 0; i < primitives.size(); i++)
            triangles.push_back(_triangles[primitives[i].id]);
        
        // nettoyage
        std::vector<Primitive>().swap(primitives);
        return root;
    }
    
    Hit intersect( const Ray& ray ) const
    {
        Hit hit;
        hit.t= ray.tmax;
        Vector invd= Vector(1 / ray.d.x, 1 / ray.d.y, 1 / ray.d.z);
        
        intersect(root, ray, invd, hit);
        return hit;
    }
    
    bool visible( const Point& p, const Point& q ) const
    {
        Ray ray(p, q);
        Vector invd= Vector(1 / ray.d.x, 1 / ray.d.y, 1 / ray.d.z);
        return !occluded(root, ray, invd);
    }
        
    bool visible( const Point& p, const Vector& v ) const
    {
        Ray ray(p, v);
        Vector invd= Vector(1 / ray.d.x, 1 / ray.d.y, 1 / ray.d.z);
        return !occluded(root, ray, invd);
    }
    
    template < typename HitFilter >
    Hit intersect( const Ray& ray, const HitFilter& isvalid ) const
    {
        Hit hit;
        hit.t= ray.tmax;
        Vector invd= Vector(1 / ray.d.x, 1 / ray.d.y, 1 / ray.d.z);
        
        intersect(root, isvalid, ray, invd, hit);
        return hit;
    }
    
    template < typename HitFilter >
    bool visible( const Point& p, const Point& q, const HitFilter& isvalid ) const
    {
        Ray ray(p, q);
        Vector invd= Vector(1 / ray.d.x, 1 / ray.d.y, 1 / ray.d.z);
        return !occluded(root, isvalid, ray, invd);
    }
    
protected:
    // construction d'un noeud, decoupe l'axe le plus long de l'englobant
    void build( const unsigned index, const int begin, const int end )
    {
        assert(index < nodes.size());
        
        BBox bounds= triangle_bounds(begin, end);
        if(end - begin < 2)
        {
            // inserer une feuille et renvoyer son indice
            nodes[index]= make_leaf(bounds, begin, end);
            return;
        }
        
        // axe le plus etire de l'englobant
        BBox cbounds= centroid_bounds(begin, end);
        Vector d= Vector(cbounds.pmin, cbounds.pmax);
        int axis;
        if(d.x > d.y && d.x > d.z)  // x plus grand que y et z ?
            axis= 0;
        else if(d.y > d.z)          // y plus grand que z ? (et que x implicitement)
            axis= 1;
        else                        // x et y ne sont pas les plus grands...
            axis= 2;

        // coupe l'englobant au milieu
        float cut= cbounds.centroid(axis);
        
        // repartit les primitives
        Primitive *pm= std::partition( primitives.data() + begin, primitives.data() + end, 
            [axis, cut]( const Primitive& primitive ) { return primitive.centroid(axis) < cut; } );
        int m= std::distance(primitives.data(), pm);
        
        // la repartition des triangles peut echouer, et tous les triangles sont dans la meme partie... 
        // forcer quand meme un decoupage en 2 ensembles 
        if(m == begin || m == end)
            m= (begin + end) / 2;
        assert(m != begin);
        assert(m != end);

    #if 1
        if(end - begin < 32)
        {
            BBox bounds_left= triangle_bounds(begin, m);
            BBox bounds_right= triangle_bounds(m, end);
            
            float area= bounds.area();
            float leaf_cost= (end - begin);
            float left_cost= bounds_left.area() * (m - begin) / area;
            float right_cost= bounds_right.area() * (end - m) / area;
            if(left_cost + right_cost + 1 > leaf_cost)  
            // +8 equilibre le cout de parcours des noeuds vs le cout de parcours des feuilles (temps d'intersection rayon / triangles), 
            /* 
                +1 construit un arbre le plus profond possible pour essayer de separer au mieux les triangles (ie les noeuds sont rapides a parcourir)
                ou avec +2, +4, +6, +8, etc. les noeuds sont plus long a parcourir et l'arbre devient naturellement moins profond avec plus de triangles par feuille...
             */
            {
                // transforme le noeud en feuille !! construire le noeud n'est pas interressant...
                nodes[index]= make_leaf(bounds, begin, end);
                return;
            }
        }
    #endif
        
        int left_index= nodes_count; 
        int right_index= left_index+1;
        nodes_count+= 2; 
        
        nodes[index]= make_node(bounds, left_index, right_index);
        
        // construire le fils gauche
        // les triangles se trouvent dans [begin .. m)
        build(left_index, begin, m);
        
        // on recommence pour le fils droit
        // les triangles se trouvent dans [m .. end)
        build(right_index, m, end);
    }
    
    
    struct Bin
    {
        BBox bounds;
        int n;
        
        Bin( ) : bounds(EmptyBox()), n(0) {}
    };
    
    // construction d'un noeud, binned sah
    void build_bins( const unsigned index, const int begin, const int end )
    {
        assert(index < nodes.size());
        
        if(end - begin < 2)
        {
            // il ne reste plus q'une seule primitive, creer une feuille et renvoyer son indice
            nodes[index]= make_leaf( triangle_bounds(begin, end), begin, end );
            return;
        }
        
        BBox cbounds= centroid_bounds(begin, end);
        Vector d= Vector(cbounds.pmin, cbounds.pmax);
        Vector invd= Vector(d.x ? 1 / d.x : 0, d.y ? 1 / d.y : 0, d.z ? 1 / d.z : 0);
        
        constexpr int bins_max= 16;
        Bin bins[bins_max][3];
        
        // repartition de toutes les primitives
        for(int i= begin; i < end; i++)
        for(int axis= 0; axis < 3; axis++)
        {
            int b= (primitives[i].centroid(axis) - cbounds.pmin(axis)) * invd(axis) * bins_max;
            if(b < 0) b= 0;
            else if(b >= bins_max) b= bins_max -1;
            
            bins[b][axis].bounds.insert(primitives[i].bounds);
            bins[b][axis].n++;
        }
        
        // calcule le cout de chaque repartition
        Bin left[3];
        Bin primitives_left[bins_max][3];
        for(int i= 0; i < bins_max; i++)
        for(int axis= 0; axis < 3; axis++)
        {
            left[axis].bounds.insert(bins[i][axis].bounds);
            left[axis].n+= bins[i][axis].n;
            primitives_left[i][axis].bounds= left[axis].bounds;
            primitives_left[i][axis].n= left[axis].n;
        }
        
        Bin right[3];
        Bin primitives_right[bins_max][3];
        for(int i= bins_max -1; i >= 0; i--)
        for(int axis= 0; axis < 3; axis++)
        {
            right[axis].bounds.insert(bins[i][axis].bounds);
            right[axis].n+= bins[i][axis].n;
            primitives_right[i][axis].bounds= right[axis].bounds;
            primitives_right[i][axis].n= right[axis].n;
        }
        
        // evalue le cout des fils
        float cost_min= FLT_MAX;
        int cost_index= -1;
        int cost_axis= -1;
        
        for(int i= 0; i < bins_max -1; i++)
        for(int axis= 0; axis < 3; axis++)
        {
            // n'evalue pas la partie constante du cout...
            float cost= primitives_left[i][axis].bounds.area() * primitives_left[i][axis].n + primitives_right[i+1][axis].bounds.area() * primitives_right[i+1][axis].n;
            
            if(cost < cost_min)
            {
                cost_min=cost;
                cost_axis= axis;
                cost_index= i;
            }
        }
        
        int m= begin;
        if(cost_index != -1)
        {
            Primitive *pm= std::partition( primitives.data() + begin, primitives.data() + end, 
                [cost_axis, cost_index, cbounds, invd, bins_max]( const Primitive &primitive )
                {
                    int b= (primitive.centroid(cost_axis) - cbounds.pmin(cost_axis)) * invd(cost_axis) * bins_max;
                    if(b < 0) b= 0;
                    else if(b >= bins_max) b= bins_max -1;
                    return b <= cost_index;
                } 
            );
            m= std::distance(primitives.data(), pm);
        }
        
        // la repartition des triangles peut echouer, et tous les triangles sont dans la meme partie... 
        // forcer quand meme un decoupage en 2 ensembles 
        if(m == begin || m == end)
            m= (begin + end) / 2;
        assert(m != begin);
        assert(m != end);
        
        // construire le noeud
        BBox bounds= EmptyBox();            // englobant du noeud, plus rapide en calculant l'union des bins...
        for(int i= 0; i < bins_max; i++)
            if(bins[i][0].n)
                bounds.insert(bins[i][0].bounds);
        
    #if 1
        if(end - begin < 32)
        {
            const BBox& bounds_left= primitives_left[cost_index][cost_axis].bounds;
            const BBox& bounds_right= primitives_right[cost_index+1][cost_axis].bounds;
            
            float invarea= 1 / bounds.area();
            float leaf_cost= (end - begin);
            float left_cost= bounds_left.area() * (m - begin) * invarea;
            float right_cost= bounds_right.area() * (end - m) *invarea;
            if(left_cost + right_cost + 1 > leaf_cost)  
            {
                // transforme le noeud en feuille !! construire le noeud n'est pas interressant...
                nodes[index]= make_leaf(bounds, begin, end);
                return;
                /* +1 construit un arbre le plus profond possible pour essayer de separer au mieux les triangles (ie les noeuds sont rapides a parcourir)
                    ou avec +2, +4, +6, +8, etc. les noeuds sont plus long a parcourir et l'arbre devient naturellement moins profond avec plus de triangles par feuille...
                
                    +8 equilibre le cout de parcours des noeuds vs le cout de parcours des feuilles (temps d'intersection rayon / triangles), 
                 */
            }
        }
    #endif
        
        int left_index= nodes_count; 
        int right_index= left_index+1;
        nodes_count+= 2; 
        
        nodes[index]= make_node(bounds, left_index, right_index);
        
        // construire le fils gauche
        // les triangles se trouvent dans [begin .. m)
        build_bins(left_index, begin, m);
        
        // on recommence pour le fils droit
        // les triangles se trouvent dans [m .. end)
        build_bins(right_index, m, end);
    }
    
    BBox triangle_bounds( const int begin, const int end )
    {
        BBox bbox= primitives[begin].bounds;
        for(int i= begin +1; i < end; i++)
            bbox.insert(primitives[i].bounds);
            
        return bbox;
    }
    
    BBox centroid_bounds( const int begin, const int end )
    {
        BBox bbox= BBox(primitives[begin].centroid);
        for(int i= begin +1; i < end; i++)
            bbox.insert(primitives[i].centroid);
            
        return bbox;
    }
    
    void intersect( const int index, const Ray& ray, const Vector& invd, Hit& hit ) const
    {
        const Node& node= nodes[index];
        if(node.leaf())
        {
            for(int i= node.leaf_begin(); i < node.leaf_end(); i++)
                if(Hit h= triangles[i].intersect(ray, hit.t))
                    hit= h;
        }
        else // if(node.internal())
        {
            const Node& left_node= nodes[node.left];
            const Node& right_node= nodes[node.right];
            
            BBoxHit left= left_node.bounds.intersect(ray, invd, hit.t);
            BBoxHit right= right_node.bounds.intersect(ray, invd, hit.t);
            if(left && right)                                       // les 2 fils sont touches par le rayon...
            {
                if(left.tmin < right.tmin)                          // parcours de gauche a droite
                {
                    intersect(node.internal_left(), ray, invd, hit);
                    intersect(node.internal_right(), ray, invd, hit);
                }
                else                                                // parcours de droite a gauche                                        
                {
                    intersect(node.internal_right(), ray, invd, hit);
                    intersect(node.internal_left(), ray, invd, hit);
                }
            }
            else if(left)                                           // uniquement le fils gauche
                intersect(node.internal_left(), ray, invd, hit);
            else if(right)
                intersect(node.internal_right(), ray, invd, hit);   // uniquement le fils droit
        }
    }
    
    
    bool occluded( const int index, const Ray& ray, const Vector& invd ) const
    {
        const Node& node= nodes[index];
        if(node.bounds.intersect(ray, invd, ray.tmax))
        {
            if(node.leaf())
            {
                for(int i= node.leaf_begin(); i < node.leaf_end(); i++)
                    if(triangles[i].intersect(ray, ray.tmax))
                        return true;
            }
            else // if(node.internal())
            {
                if(occluded(node.internal_left(), ray, invd) || occluded(node.internal_right(), ray, invd))
                    return true;
            }
        }
        
        return false;
    }
    
    
    template< typename HitFilter >
    void intersect( const int index, const HitFilter& isvalid, const Ray& ray, const Vector& invd, Hit& hit ) const
    {
        const Node& node= nodes[index];
        if(node.leaf())
        {
            for(int i= node.leaf_begin(); i < node.leaf_end(); i++)
            {
                Hit h= triangles[i].intersect(ray, hit.t);
                if(h && isvalid(h))
                    hit= h;
            }
        }
        else // if(node.internal())
        {
            const Node& left_node= nodes[node.left];
            const Node& right_node= nodes[node.right];
            
            BBoxHit left= left_node.bounds.intersect(ray, invd, hit.t);
            BBoxHit right= right_node.bounds.intersect(ray, invd, hit.t);
            if(left && right)                                       // les 2 fils sont touches par le rayon...
            {
                if(left.tmin < right.tmin)                          // parcours de gauche a droite
                {
                    intersect(node.internal_left(), isvalid, ray, invd, hit);
                    intersect(node.internal_right(), isvalid, ray, invd, hit);
                }
                else                                                // parcours de droite a gauche                                        
                {
                    intersect(node.internal_right(), isvalid, ray, invd, hit);
                    intersect(node.internal_left(), isvalid, ray, invd, hit);
                }
            }
            else if(left)                                           // uniquement le fils gauche
                intersect(node.internal_left(), isvalid, ray, invd, hit);
            else if(right)
                intersect(node.internal_right(), isvalid, ray, invd, hit);   // uniquement le fils droit
        }
    }
    
    template< typename HitFilter >
    bool occluded( const int index, const HitFilter& isvalid, const Ray& ray, const Vector& invd ) const
    {
        const Node& node= nodes[index];
        if(node.bounds.intersect(ray, invd, ray.tmax))
        {
            if(node.leaf())
            {
                for(int i= node.leaf_begin(); i < node.leaf_end(); i++)
                {
                    Hit h= triangles[i].intersect(ray, ray.tmax);
                    if(h && isvalid(h))
                        return true;
                }
            }
            else // if(node.internal())
            {
                if(occluded(node.internal_left(), isvalid, ray, invd) || occluded(node.internal_right(), isvalid, ray, invd))
                    return true;
            }
        }
        
        return false;
    }
};


//!< decale un point le long de la normale, evite l'auto intersection / ombrage...
//!< cf https://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/group__precision.html
static inline
Point offset_point( const Point& p, const Vector& pn )
{
    // evalue l'epsilon relatif du point d'intersection
    float pmax= std::max(std::abs(p.x), std::max(std::abs(p.y), std::abs(p.z)));
    float pe= pmax * FLT_EPSILON;
    
    // *32 pour tenir compte des autres approximations / erreurs de calculs
    return p + 32*pe*pn;
}
