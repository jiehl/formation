
#pragma once

#include "fresnel.h"

// cf "generating a consistently oriented tangent space" 
// http://people.compute.dtu.dk/jerf/papers/abstracts/onb.html
// cf "Building an Orthonormal Basis, Revisited", Pixar, 2017
// http://jcgt.org/published/0006/01/01/
struct World
{
    World( ) : t(), b(), n() {}
    World( const Vector& _n ) : n(_n) 
    {
        float sign= std::copysign(1.0f, n.z);
        float a= -1.0f / (sign + n.z);
        float d= n.x * n.y * a;
        t= Vector(1.0f + sign * n.x * n.x * a, sign * d, -sign * n.x);
        b= Vector(d, sign + n.y * n.y * a, -n.y);        
    }
    
    // transforme le vecteur du repere local vers le repere du monde
    Vector operator( ) ( const Vector& local )  const { return local.x * t + local.y * b + local.z * n; }
    
    // transforme le vecteur du repere du monde vers le repere local
    Vector local( const Vector& global ) const { return Vector(dot(global, t), dot(global, b), dot(global, n)); }
    
    Vector t;
    Vector b;
    Vector n;
};


struct Brdf
{
    World world;
    Color diffuse;
    Color F0;
    float metallic;
    float alpha;
    
    Brdf( ) = default;
    
    // opaque
    Brdf( const Vector& ng, const Color& _color, const float _metallic, const float _roughness ) : world(ng), 
        diffuse( (1 - _metallic) * _color ), 
        F0( (1 - _metallic) * Color(0.04) + _metallic * _color ), 
        metallic(_metallic), alpha(_roughness*_roughness)
    {}
    
    Color f( const Vector& l, const Vector& o ) const
    {
        Vector h= normalize(o + l);
        float ndoth= std::max(float(0), dot(world.n, h));
        
        float ndotl= std::max(float(0), dot(world.n, l));
        float ndoto= std::max(float(0), dot(world.n, o));
        
        // meme hemisphere
        if(ndoto <= 0 || ndoth <= 0 || ndotl <= 0)
            return Black();
        float hdoto= dot(o, h);
        float hdotl= dot(l, h);
        if(hdoto <= 0 || hdotl <= 0)
            return Black();
        
        float D= ggx_d( alpha, ndoth);
        float G2= smith_g2( alpha, ndotl, ndoto);
        Color F= schlick_fresnel( F0, hdotl );
        
        // brdf
        Color fr_m= (F * D * G2) / (4 * ndoto * ndotl);
        
        //~ Color disney_diffuse= diffuse / float(M_PI) * (1 - std::pow(1 - ndotl, 5) / 2) * (1 - std::pow(1 - ndoto, 5) / 2);
        Color disney_diffuse= diffuse / float(M_PI);
        Color fr_d= (Color(1) - F) * disney_diffuse + (F * D * G2) / (4 * ndoto * ndotl);
        Color fr= (1 - metallic) * fr_d + metallic * fr_m;
        
        return fr;
    }

    Vector sample( const float u1, const float u2, const float u3, const Vector& o ) const
    {
        float ndoto= dot(world.n, o);
        if(ndoto <= 0)
            return Vector();
        
        // evalue le fresnel dans la direction miroir pour choisir le lobe....
        //~ float f= schlick_fresnel(F0, ndoto).max();
        float f= 0.5;
        if(u1 < f)
        {
            // genere une direction ~ cos theta / pi
            float phi= float(2*M_PI) * u2;
            //~ float cos_theta= std::sqrt(u3);
            float cos_theta= u3;    // 1 / 2pi
            float sin_theta= std::sqrt(1 - std::min(float(1.0), cos_theta*cos_theta));
            return world( Vector(std::cos(phi) * sin_theta, std::sin(phi) * sin_theta, cos_theta) );
        }
        else
        {
            // genere une direction ~ D
            //~ float walter_alpha= (float(1.2) - float(0.2)*std::sqrt(ndoto)) * alpha;
            //~ Vector h= world( ggx_sample( walter_alpha, u2, u3 ) );
            //~ Vector h= world( ggx_sample( alpha, u2, u3 ) ); // walter
            
            Vector h= world( vndf_sample( world.local(o), alpha, u2, u3 ) ); // dupuy vndf sampling
            // construit la direction reflechie
            return reflect(o, h); 
        }
    }
    
    float pdf( const Vector& l, const Vector& o ) const
    {
        float ndotl= std::max(float(0), dot(world.n, l));
        float ndoto= std::max(float(0), dot(world.n, o));
        if(ndoto <= 0 || ndotl <= 0)
            return 0;
            
        Vector h= normalize(o + l);
        float ndoth= std::max(float(0), dot(world.n, h));
        float hdoto= dot(o, h);
        float hdotl= dot(l, h);
        if(ndoth <= 0 || hdoto <= 0 || hdotl <= 0)
            return 0;
            
        //~ float f= schlick_fresnel(F0, ndoto).max();
        float f= 0.5;
        float d= 1 / float(2*M_PI); // ndotl / float(M_PI);
        //~ float m= ggx_d(alpha, ndoth) * ndoth / hdoto / 4;   // walter weight, cf ggx_sample()
        float m= ggx_d(alpha, ndoth) * smith_g1(alpha, ndoth) / (4 * ndoto);    // dupuy weight, cd vndf_sample()
        
        return (1 - f)*d + f*m;
    }
    
protected:
    float ggx_d( const float alpha, const float cos_theta ) const
    {
        assert(cos_theta > 0);
        float d= alpha / (1 + cos_theta*cos_theta * (alpha*alpha - 1));
        return d*d / float(M_PI);
    }
    
    float smith_g1( const float alpha, const float cos_theta ) const
    {
        return 1 / (1 + ggx_lambda(alpha, cos_theta));
    }
    
    float smith_g2( const float alpha, const float cos_theta, const float cos_theta_o ) const
    {
        return 1 / (1 + ggx_lambda(alpha, cos_theta) + ggx_lambda(alpha, cos_theta_o));
    }
    
    float ggx_lambda( const float alpha, const float cos_theta ) const
    {
        assert(cos_theta > 0);
        float cos2_theta= cos_theta * cos_theta;
        float tan2_theta= std::max(float(0), 1 - cos2_theta) / cos2_theta;
        return (std::sqrt(1 + alpha*alpha * tan2_theta) -1) / 2;
    }
    
    // from "Microfacet Models for Refraction through Rough Surfaces", walter 2007
    // cf https://www.graphics.cornell.edu/~bjw/microfacetbsdf.pdf
    Vector ggx_sample( const float alpha, const float u1, const float u2 ) const
    {
        float theta= std::atan( alpha * std::sqrt(u1) / std::sqrt(1 - u1) );
        float sin_theta= std::sin(theta);
        float cos_theta= std::cos(theta);
        
        float phi= u2 * float(2*M_PI);
        return Vector(std::cos(phi) * sin_theta, std::sin(phi) * sin_theta, cos_theta);
    }
    
    // from "Sampling Visible GGX Normals with Spherical Caps", dupuy 2023
    // cf https://arxiv.org/pdf/2306.05044.pdf
    
    // Sample the GGX VNDF
    Vector vndf_sample( const Vector& wi, const float alpha, const float u1, const float u2 ) const
    {
        // warp to the hemisphere configuration
        Vector wi_std = normalize(Vector(wi.x * alpha, wi.y * alpha, wi.z));
        // sample the hemisphere
        Vector wm_std = sample_vndf_hemisphere(wi_std, u1, u2);
        // warp back to the ellipsoid configuration
        Vector wm = normalize(Vector(wm_std.x * alpha, wm_std.y * alpha, wm_std.z));
        // return final normal
        return wm;
    }    
    
    // Helper function: sample the visible hemisphere from a spherical cap
    Vector sample_vndf_hemisphere( const Vector& wi, const float u1, const float u2 ) const
    {
        // sample a spherical cap in (-wi.z, 1]
        float phi = u1 * float(2*M_PI);
        // float z = fma((1.0f - u.y), (1.0f + wi.z), -wi.z);
        float z = 1 - u2 - u2 * wi.z;
        float sin_theta = std::sqrt(clamp(1 - z*z, 0, 1));
        float x = sin_theta * std::cos(phi);
        float y = sin_theta * std::sin(phi);
        Vector c = Vector(x, y, z);
        // compute halfway direction;
        Vector h = c + wi;
        // return without normalization as this is done later (see line 25)
        return h;
    }
    
    float clamp( const float x, const float xmin, const float xmax ) const
    {
        if(x < xmin) return xmin;
        if(x > xmax) return xmax;
        return x;
    }
};

