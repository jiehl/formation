
#pragma once

#include <unordered_map>
#include <vector>

#include "vec.h"
#include "color.h"


// triangle abc + emission
struct Source
{
    Point a, b, c;
    Color emission;
    Vector n;   // normale
    float area; // aire
    
    Source( ) : a(), b(), c(), emission(), n(), area() {}
    
    Source( const Point& _a, const Point& _b, const Point& _c, const Color& _color ) : a(_a), b(_b), c(_c), emission(_color)
    {
       // normale geometrique du triangle abc, produit vectoriel des aretes ab et ac
        Vector ng= cross(Vector(a, b), Vector(a, c));
        n= normalize(ng);
        area= length(ng) / 2;
        
        assert(area * emission.max() > 0);
    }
    
    Point sample( const float u1, const float u2 ) const
    {
    #if 0
        // cf GI compendium eq 18, https://people.cs.kuleuven.be/~philip.dutre/GI/TotalCompendium.pdf
        // ou PBRT http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/2D_Sampling_with_Multidimensional_Transformations.html#SamplingaTriangle
        float r1= std::sqrt(u1);
        float alpha= 1 - r1;
        float beta= (1 - u2) * r1;
        float gamma= u2 * r1;
        
        return alpha*a + beta*b + gamma*c;
    #else
        
        // "A Low-Distortion Map Between Triangle and Square"
        // cf https://pharr.org/matt/blog/2019/03/13/triangle-sampling-1.5.html
        // cf https://drive.google.com/file/d/1J-183vt4BrN9wmqItECIjjLIKwm29qSg/view
        float b0= u1 / 2;
        float b1= u2 / 2;
        float offset= b1 - b0;
        if(offset > 0)
            b1= b1 + offset;
        else
            b0= b0 - offset;
        float b2= 1 - b0 - b1;
        
        return b0*a + b1*b + b2*c;
    #endif
    }
    
    float pdf( const Point& p ) const
    {
        // todo : devrait renvoyer 0 pour les points a l'exterieur du triangle...
        return 1 / area;
    }
};


// ensemble de sources
struct Sources
{
    std::vector<Source> sources;
    std::vector<float> cdf;
    float area;         // aire totale des sources
    
    void build( const std::vector<Source>& triangles )
    {
        area= 0;
        sources.clear();
        for(unsigned id= 0; id < triangles.size(); id++)
        {
            if(triangles[id].area > 0)
            {
                area= area + triangles[id].area;
                
                sources.push_back( triangles[id] );
                cdf.push_back( area );
            }
        }
        
        printf("%d emissive triangles...\n", int(sources.size()));
    }
    
    int size( ) const { return int(sources.size()); }
    const Source& operator() ( const int id ) const { assert(id >= 0); assert(id < int(sources.size())); return sources[id]; }
    
    const Source& sample( const float& u1 ) const
    {
        float r= u1 * area;
        
        // recherche dichotomique
        int begin= 0;
        int end= int(cdf.size()) -1;
        while(begin < end)
        {
            int m= (begin + end) / 2;
            if(cdf[m] < r)
               begin= m+1;
            else
               end= m;
        }
        assert(begin >= 0 && begin < int(cdf.size()));
        
        return sources[begin];
    }
    
    const Source& sample_reuse( float& u1 ) const
    {
        float r= u1 * area;
        
        // recherche dichotomique
        int begin= 0;
        int end= int(cdf.size()) -1;
        while(begin < end)
        {
            int m= (begin + end) / 2;
            if(cdf[m] < r)
               begin= m+1;
            else
               end= m;
        }
        assert(begin >= 0 && begin < int(cdf.size()));
        
        #if 0
        {
            // check
            int i;
            for(i= 0; i < int(cdf.size()); i++)
                if(r <= cdf[i])
                    break;
            
            if(i < int(cdf.size()) && begin != i)
            {
                #pragma omp critical
                {
                    printf("i %u %f < %f < %f\n", i, (i-1 >= 0) ? cdf[i-1] : 0, r, cdf[i]);
                    printf("b %u %f < %f < %f\n", begin, (begin-1 >= 0) ? cdf[begin-1] : 0, r, cdf[begin]);
                    exit(1);
                }
            }
        }
        #endif
        
        // renormalise u1
        if(begin > 0)
            u1= (r - cdf[begin-1]) / (cdf[begin] - cdf[begin-1]);
        else
            u1= r / cdf[begin];
        
        return sources[begin];
    }
    
    float pdf( const Source& s ) const
    {
        return s.area / area;
    }
};
