
#pragma once

#include <cassert>
#include <random>

#include "vec.h"
#include "mat.h"
#include "gltf/gltf.h"
#include "gltf/bvh.h"
#include "gltf/brdf.h"
#include "gltf/sources.h"
#include "gltf/texture.h"


struct Scene
{
    GLTFScene gltf;
    GLTFCamera camera;
    
    BVH bvh;
    Sources sources;
    
    // textures
    std::vector<Texture> textures;
    
    // filtrage
    Transform view;
    int image_width, image_height;
    float pixel_cone_angle;
    
    // renvoie la matiere du point d'intersection
    const GLTFMaterial& material( const Hit& hit ) const;
    float triangle_area( const Hit& hit ) const;
    
    // renvoie la brdf du point d'intersection
    Brdf brdf( const Hit& hit, const Vector& n ) const;
    
    // renvoie les coordonnees de texture interpolee au point d'intersection + lod
    vec3 texcoords_lod( const Hit& hit ) const;
    
    // renvoie la normale interpolee au point d'intersection
    Vector normal( const Hit& hit ) const;
    
    // renvoie les coordonnees de texture interpolees au point d'intersection
    vec2 texcoords( const Hit& hit ) const;
    
    // renvoie le point d'intersection interpole sur le triangle.
    Point position( const Hit& hit ) const;
    
    // renvoie le point d'intersection interpole sur le rayon.
    Point position( const Hit& hit, const Ray& ray ) const { return ray(hit); }
    
    //~ Source& source( const Hit& hit );   // todo
    
    bool visible( const Point& p, const Point& q ) const { return bvh.visible(p, q); }
    bool visible( const Point& p, const Vector& d ) const { return bvh.visible(p, d); }
    Hit intersect( const Ray& ray ) const { return bvh.intersect(ray); }
    
    // anyhit filter, cf alpha_filter
    template< typename Filter > bool visible( const Point& p, const Vector& d, const Filter& filter ) const { return bvh.visible(p, d, filter); }
    template< typename Filter > Hit intersect( const Ray& ray, const Filter& filter ) const { return bvh.intersect(ray, filter); }
};


Scene read_scene( const char *filename );
int read_textures( const char *filename, Scene& scene, const int width, const int height );
