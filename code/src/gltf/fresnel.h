
#pragma once

#include "vec.h"
#include "color.h"

//!< renvoie la direction miroir de w, par rapport a n.
Vector reflect( const Vector& w, const Vector& n );

//!< verifie qu'une refraction est possible.
bool can_refract( const float ni, const float nt, const Vector& w, const Vector& n );

//!< renvoie la direction refractee de w, par rapport a n.
Vector refract( const float ni, const float nt, const Vector& w, const Vector& n );

//!< renvoie le coefficient de reflexion de fresnel.
float fresnel( const float ni, const float nt, const Vector& w, const Vector& n );

//!< renvoie l'approximation du coefficient de fresnel. reflexion.
Color schlick_fresnel( const Color& F0, const float cos_theta );

//!< renvoie l'approximation du coefficient de fresnel. reflexion et refraction.
Color schlick_fresnel( const float ni, const float nt, const Vector& w, const Vector& n );

//!< renvoie F0, coeff de Fresnel a incidence normale.
float schlick_f0( const float ior );

//!< renvoie F0, coeff de Fresnel a incidence normale.
float schlick_f0( const float ni, const float nt );

//!< renvoie estime un coefficient de refraction a partir de F0.
float schlick_ior( const float f0 );

