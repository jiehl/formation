
#pragma once

//! \addtogroup image utilitaires pour manipuler des images
///@{


#include "image.h"

//! charge une image .bmp .tga .jpeg .png ou .hdr
Image read_image( const char *filename, const bool flipY= true );

//! enregistre une image au format .png
bool write_image( const Image& image, const char *filename, const bool flipY= true );
//! enregistre une image au format .png
bool write_image_png( const Image& image, const char *filename, const bool flipY= true );
//! enregistre une image au format .bmp
bool write_image_bmp( const Image& image, const char *filename, const bool flipY= true );
//! enregistre une image au format .hdr
bool write_image_hdr( const Image& image, const char *filename, const bool flipY= true );

//! enregistre une image au format .pfm
bool write_image_pfm( const Image& image, const char *filename, const bool flipY= true );
//! charge une image .pfm
Image read_image_pfm( const char *filename, const bool flipY= true );

bool pfm_image( const char *filename );


//! raccourci pour write_image_png(tone(image, range(image)), "image.png")
bool write_image_preview( const Image& image, const char *filename, const bool flipY= true );

//! evalue l'exposition d'une image.
float range( const Image& image );
//! correction de l'exposition d'une image + transformation rgb -> srgb.
Image tone( const Image& image, const float saturation );

//! reduit les dimensions d'une image.
Image downscale( const Image& image );

///@}
