
#include <cfloat>

#include "color.h"
#include "image.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


float range( const Image& image )
{
    float gmin= FLT_MAX;
    float gmax= 0;
    for(unsigned i= 0; i < image.size(); i++)
    {
        Color color= image(i);
        float g= color.r + color.g + color.b;
        
        if(g < gmin) gmin= g;
        if(g > gmax) gmax= g;
    }
    
    // \todo utiliser un log2 pour l'histogramme
    int bins[100] = {};
    for(unsigned i= 0; i < image.size(); i++)
   {
        Color color= image(i);
        float g= color.r + color.g + color.b;
        
        int b= (g - gmin) * 100 / (gmax - gmin);
        if(b >= 99) b= 99;
        if(b < 0) b= 0;
        bins[b]++;
    }
    
    float saturation= 0;
    float qbins= 0;
    for(unsigned i= 0; i < 100; i++)
    {
        qbins= qbins + float(bins[i]) / float(image.size());
        if(qbins > .75f)
            return gmin + float(i+1) / 100 * (gmax - gmin);
    }
    
    return gmax;
}


Image tone( const Image& image, const float saturation )
{
    Image tmp(image.width(), image.height());
    
    float k= 1 / std::pow(saturation, 1 / float(2.2));
    for(unsigned i= 0; i < image.size(); i++)
    {
        Color color= image(i);
        if(std::isnan(color.r) || std::isnan(color.g) || std::isnan(color.b))
            // marque les pixels pourris avec une couleur improbable...            
            color= Color(1, 0, 1);
        else
            // sinon transformation rgb -> srgb, mais preserve alpha, pas de transformation...
            color= srgb(k * color);
        
        tmp(i)= Color(color, 1);
    }
    
    return tmp;
}


Image read_image( const char *filename, const bool flipY )
{
    stbi_set_flip_vertically_on_load(flipY);
    
    if(!stbi_is_hdr(filename))
    {
        int width, height, channels;
        unsigned char *data= stbi_load(filename, &width, &height, &channels, 4);
        if(!data)
        {
            printf("[error] loading '%s'...\n", filename);
            return {};
        }
        
        Image image(width, height);
        for(unsigned i= 0, offset= 0; i < image.size(); i++, offset+= 4)
        {
            Color pixel= Color( 
                data[offset], 
                data[offset + 1],
                data[offset + 2],
                data[offset + 3]) / 255;
            image(i)= pixel;
        }
        
        stbi_image_free(data);
        return image;
        
        // \todo utiliser stbi_loadf() dans tous les cas, + parametres de conversion
        // cf read_gltf_images()...
        //     stbi_ldr_to_hdr_scale(1.0f);
        //     stbi_ldr_to_hdr_gamma(2.2f);
        
    }
    else
    {
        int width, height, channels;
        float *data= stbi_loadf(filename, &width, &height, &channels, 4);
        if(!data)
        {
            printf("[error] loading '%s'...\n", filename);
            return {};
        }
        
        Image image(width, height);
        for(unsigned i= 0, offset= 0; i < image.size(); i++, offset+= 4)
        {
            Color pixel= Color( 
                data[offset], 
                data[offset + 1],
                data[offset + 2],
                data[offset + 3]);
            image(i)= pixel;
        }
        
        stbi_image_free(data);
        return image;
    }
    
    return {};
}

inline float clamp( const float x, const float min, const float max )
{
    if(x < min) return min;
    else if(x > max) return max;
    else return x;
}


bool write_image_png( const Image& image, const char *filename, const bool flipY )
{
    if(image.size() == 0)
        return false;
    
    std::vector<unsigned char> tmp(image.width()*image.height()*4);
    for(unsigned i= 0, offset= 0; i < image.size(); i++, offset+= 4)
    {
        Color pixel= image(i) * 255;
        tmp[offset   ]= clamp(pixel.r, 0, 255);
        tmp[offset +1]= clamp(pixel.g, 0, 255);
        tmp[offset +2]= clamp(pixel.b, 0, 255);
        tmp[offset +3]= clamp(pixel.a, 0, 255);
    }
    
    stbi_flip_vertically_on_write(flipY);
    return stbi_write_png(filename, image.width(), image.height(), 4, tmp.data(), image.width() * 4) != 0;
}

bool write_image( const Image& image, const char *filename, const bool flipY )
{
    return write_image_png(image, filename, flipY );
}

bool write_image_bmp( const Image& image, const char *filename, const bool flipY )
{
    if(image.size() == 0)
        return false;
    
    std::vector<unsigned char> tmp(image.width()*image.height()*4);
    for(unsigned i= 0, offset= 0; i < image.size(); i++, offset+= 4)
    {
        Color pixel= image(i) * 255;
        tmp[offset   ]= clamp(pixel.r, 0, 255);
        tmp[offset +1]= clamp(pixel.g, 0, 255);
        tmp[offset +2]= clamp(pixel.b, 0, 255);
        tmp[offset +3]= clamp(pixel.a, 0, 255);
    }
    
    stbi_flip_vertically_on_write(flipY);
    return stbi_write_bmp(filename, image.width(), image.height(), 4, tmp.data()) != 0;
}

bool write_image_hdr( const Image& image, const char *filename, const bool flipY )
{
    if(image.size() == 0)
        return false;
    
    stbi_flip_vertically_on_write(flipY);
    return stbi_write_hdr(filename, image.width(), image.height(), 4, image.data()) != 0;
}


bool write_image_pfm( const Image& image, const char *filename, const bool flipY )
{
    if(image.size() == 0)
        return false;
    
    FILE *out= fopen(filename, "wb");
    if(out == nullptr)
    {
        printf("[error] writing pfm image '%s'...\n", filename);
        return -1;
    }
    
    fprintf(out, "PF\xa%d %d\xa-1\xa", image.width(), image.height());
    
    for(int y= 0; y < image.height(); y++)
    for(int x= 0; x < image.width(); x++)
    {
        Color pixel= image(x, y);
        fwrite(&pixel.r, sizeof(float), 3, out);
    }
    fclose(out);
    
    return true;
}

bool pfm_image( const char *filename )
{
    const char *ext= strrchr(filename, '.');
    if(ext == nullptr)
        return false;
        
    return strcmp(ext, ".pfm") == 0;
}

Image read_image_pfm( const char *filename, const bool flipY )
{
    FILE *in= fopen(filename, "rb");
    if(in == nullptr)
    {
        printf("[error] loading pfm image '%s'...\n", filename);
        return {};
    }
    
    int w, h;
    float endian= 0;
    if(fscanf(in, "PF\xa%d %d\xa%f[^\xa]", &w, &h, &endian) != 3 || endian != -1)
    {
        printf("[error] loading pfm image '%s'...\n", filename);
        return Image();
    }
    
    // saute la fin de l'entete
    unsigned char c= fgetc(in);
    while(c != '\xa')
        c= fgetc(in);
    
    // pourquoi aussi tordu ? fscanf(in, "PF\n%d %d\n%f\n") consomme les espaces apres le \n... ce qui est un poil genant pour relire les floats...
    
    printf("loading pfm image '%s' %dx%d...\n", filename, w, h);
    
    std::vector<float> pixels(w*h*3);
    if(fread(pixels.data(), sizeof(float)*3, w*h, in) != size_t(w*h))
    {
        printf("[error] reading pfm image '%s'...\n", filename);
        fclose(in);
        return {};
    }
    fclose(in);
    
    Image image(w, h);
    if(flipY)
    {
        for(int y= 0; y < h; y++)
        {
            for(int x= 0; x < w; x++)
            {
                unsigned offset= (h-1 - y) * w * 3 + x *3;
                image(x, y)= Color(pixels[offset], pixels[offset+1], pixels[offset+2]);
            }
        }
    }
    else
    {
        for(unsigned i= 0, offset= 0; i < image.size(); i++, offset+= 3)
            image(i)= Color(pixels[offset], pixels[offset+1], pixels[offset+2]);
    }
    
    return image;
}


bool write_image_preview( const Image& image, const char *filename, const bool flipY )
{
    if(image.size() == 0)
        return false;
    
    Image tmp= tone(image, range(image));
    return write_image_png(tmp, filename, flipY);
}


Image downscale( const Image& image )
{
    Image mip(std::max(1, image.width()/2), std::max(1, image.height()/2));
    
    for(int y= 0; y < mip.height(); y++)
    for(int x= 0; x < mip.width(); x++)
        mip(x, y)= ( 
              image(2*x, 2*y) 
            + image(2*x+1, 2*y)
            + image(2*x, 2*y+1) 
            + image(2*x+1, 2*y+1) 
            ) / 4;
    
    return mip;
}
