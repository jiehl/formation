
#pragma once

#include "vec.h"

//! \addtogroup image
///@{

//! \file
//! manipulation de couleurs

//! representation d'une couleur (rgba) transparente ou opaque.
struct Color
{
    //! constructeur par defaut.
    Color( ) : r(0), g(0), b(0), a(1) {}
    explicit Color( const float _r, const float _g, const float _b, const float _a= 1 ) : r(_r), g(_g), b(_b), a(_a) {}
    explicit Color( const float _value ) : r(_value), g(_value), b(_value), a(1) {}
    
    //! cree une couleur avec les memes composantes que color, mais remplace sa composante alpha (color.r, color.g, color.b, alpha).
    Color( const Color& color, const float alpha ) : r(color.r), g(color.g), b(color.b), a(alpha) {}  // remplace alpha.
    
    Color( const Vector& v ) : r(v.x), g(v.y), b(v.z), a(1) {}
    
    float grey( ) const;
    float max( ) const;

    bool zero( ) const { return r == 0 && g == 0 && b == 0; }
    bool black( ) const { return r == 0 && g == 0 && b == 0; }
    
    float r, g, b, a;
};


//! utilitaire. transformation gamma. 
Color gamma( const Color& color, const float g );
//! transformation gamma : rgb lineaire vers srgb
Color srgb( const Color& color );
//! transformation gamma : srgb vers rgb lineaire
Color linear( const Color& color );


//! utilitaire. renvoie une couleur noire.
Color Black( );
//! utilitaire. renvoie une couleur blanche.
Color White( );
//! utilitaire. renvoie une couleur rouge.
Color Red( );
//! utilitaire. renvoie une couleur verte.
Color Green( );
//! utilitaire. renvoie une couleur bleue.
Color Blue( );
//! utilitaire. renvoie une couleur jaune.
Color Yellow( );

Color operator+ ( const Color& a, const Color& b );
Color operator- ( const Color& a, const Color& b );
Color operator- ( const Color& c );
Color operator* ( const Color& a, const Color& b );
Color operator* ( const Color& c, const float k );
Color operator* ( const float k, const Color& c );
Color operator/ ( const Color& a, const Color& b );
Color operator/ ( const float k, const Color& c );
Color operator/ ( const Color& c, const float k );

///@}
