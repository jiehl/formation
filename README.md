![](code/teaser.png)

cloner le depot : git clone https://gitlab.liris.cnrs.fr/jiehl/formation/

la base de code compile sans dépendances.

ouvrez la solution visual studio dans build/


plusieurs scènes de tests au format .glTF sont également disponibles : cf les fichiers *.glb

# TP
il y a 2 codes de tests dans `projects` :

- `ao.cpp` pour prendre le code en main, c'est un calcul assez direct d'ambiant occlusion,
- `path.cpp`, un path tracer tout simple qu'il va falloir améliorer.

les 2 codes produisent des images .png classiques, éventuellement mal exposées, mais également des images HDR au format .pfm. vous pouvez utiliser [Tev](https://github.com/Tom94/tev) pour les visualiser.

il est possible de changer de sampler pour faire quelques comparaisons, il suffit de modifier le typedef 
au début des sources. les samplers eux mêmes sont dans :

- `projects/sampler.h` : il y a un générateur standard c++ et 2 générateurs basés compteurs,
- `projects/sampler_sobol.h` : la séquence sobol classique, avec max 128 dimensions, la version avec une évaluation "rapide" dans l'ordre des codes de gray, et les 2 acceptent un scrambling simple, random digit shift,
- `projects/owen_fast.h` : les permutations d'owen (à utiliser sur un échantillon sobol).

voila quelques suggestions, à faire (ou pas) dans l'ordre que vous préferrez.


## réduction de dimensions
vous pouvez appliquer les différentes stratégies de réduction de dimensions au path tracer : 

- passer de 3d à 2d dans la sélection des sources, et dans la génération des directions en fonction des matières,
- réutiliser les mêmes nombres aléatoires pour estimer les 2 intégrales, cf la fonction `direct_mis()`.

le nombre de dimensions comsommées est affiché à la fin du calcul.

j'ai "laissé" la dernière amélioration en place, qui permet d'économiser un rayon par rebond et par pixel.

les matières sont dans `src/gltf/brdf.h` et les sources de lumières (uniquement des triangles émissifs) se trouvent dans `src/gltf/sources.h`.


## sélection des sources
vous pouvez également modifier la stratégie de sélection des sources dans `src/gltf/sources.h`. la solution actuelle construit une variable aléatoire discrète et inverse sa cdf pour sélectionner une source proportionnellement à son aire.

et par rapport à son angle solide ? c'est mieux ou pas ? dans quels cas ?

cf [wikipedia](https://en.wikipedia.org/wiki/Solid_angle#Tetrahedron) pour calculer l'angle solide d'un triangle


## multiple importance sampling
vous pouvez facilement modifier l'heuristique utilisée pour combiner les 2 stratégies dans la fonction `direct_mis()`.
ie utiliser balance au lieu de power comme c'est le cas dans le code.

il y a 2 estimateurs MIS, la version "multi-sample" et la version "one-sample". que faut-il modifier pour écrire la version "one-sample". quelles sont les différences en temps de rendu, en erreur ?


## permutations d'Owen
il y a un bug numérique dans les permutations d'owen, cf `projects/owen_fast.h`. vous pouvez vous en rendre compte en comparant les images calculées avec Owen et OwenFast pour peu d'échantillons (< 16).

de manière assez surprenante le code de construction des permutations est correct, le générateur de nombre aléatoire (utilisé pour les décisions aléatoires de permuter chaque bit), aussi, à un détail près...


## roulette russe ?
comment terminer les chemins sans introduire de biais systématique en limitant brutalement le nombre de rebonds ?

une solution interressante est présentée dans ["Adjoint-Driven Russian Roulette and Splitting in Light Transport Simulation"](https://cgg.mff.cuni.cz/~jaroslav/papers/2016-adrrs/index.htm), 2016

il faut le lire calmement, mais tout ce que dit l'article est qu'il faut calculer chaque rebond avec un nombre d'échantillon proportionnel à la contribution du rebond à l'image complète (ie la somme des rebonds). 

comment peut-on estimer une somme de plusieurs intégrales avec Monte Carlo ? ça vous rappelle quelque chose ?
la vraie difficulté est due à la construction progressive des chemins. si le 2ieme rebond doit être évalué avec plus d'échantillons que le premier, il faut utiliser du splitting, pour obtenir le bon nombre de chemins de cette longueur. mais si le rebond suivant doit être évalué avec moins d'échantillons, il faut utiliser une roulette russe pour terminer brutalement certains chemins et conserver le bon nombre de chemins.


## séquence de Sobol'
re-écrivez la boucle principale pour utiliser la séquence de Sobol' globale, ie ce sont les dimensions 0, 1 qui décident quel pixel / point du plan image échantilloner.

utilisez une image carrée dont le coté est une puissance de 2. la stratification naturelle des 2 premières dimensions permet de visiter chaque pixel de l'image.



## Z Sampler
une modification des permutations d'Owen permet de produire des échantillons à peu près "Blue noise". 

le principe est présenté dans cet article :
["Screen-Space Blue-Noise Diffusion of Monte Carlo Sampling Error via Hierarchical Ordering of Pixels"](http://abdallagafar.com/publications/zsampler/) 2020.

l'idée est de construire le code de Morton des indices des échantillons et d'utiliser une permutation 4D de ce code.

il faut également écrire la séquence globale, cf l'exercice précédent.

il est également possible de construire une fonction de hachage comme dans le cas classique. les détails sont sur le blog ["Psychopath renderer"](https://psychopath.io/) :

- présentation ["Owen Scrambling Based Blue-Noise Dithered Sampling"](https://psychopath.io/post/2022_07_24_owen_scrambling_based_dithered_blue_noise_sampling)
- fonction de hachage pour les permutations classiques ["Building a Better LK Hash"](https://psychopath.io/post/2021_01_30_building_a_better_lk_hash)
- fonction de hachage pour les permutations 4d et le Z sampler ["A Fast Hash For Base-4 Owen Scrambling"](https://psychopath.io/post/2022_08_14_a_fast_hash_for_base_4_owen_scrambling)


## convergence
`projects/errors.cpp` prend en paramètre une image de référence et une série d'images calculées avec un nombre croissant d'échantillons et calcule l'erreur de chaque image par rapport à la réference. 

le script `scripts/ao.txt` permet d'automatiser le rendu des images, de calculer leurs erreurs et de produire le graphe avec gnuplot. c'est pour l'instant un script bash qui ne marchera pas sous windows.


## filtrage
il est très simple de filter une image avec [OIDN](https://www.openimagedenoise.org/). c'est prévu dans le script, ainsi que le calcul d'erreur.

les 2 codes de départ exportent aussi les normales et les albedos de chaque pixel pour aider le denoiser.




